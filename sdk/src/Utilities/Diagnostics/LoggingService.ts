import {Injectable} from '@angular/core';

import {WebApi} from '../MessageBus/WebApi';

export type LogLevel = "notice" | "warning" | "error";

export interface Log {
    level: LogLevel,
    datetime: number,
    text: string,
    exception?: Error
}

export interface ILogging {
    logBatch(logs: Log[]): Promise<void>
}

@Injectable()
export class Logging extends WebApi implements ILogging {

    constructor() {super('logging');}

    logBatch(logs: Log[]): Promise<void> {
        return this.call<void>('logBatch', logs);
    }
}


