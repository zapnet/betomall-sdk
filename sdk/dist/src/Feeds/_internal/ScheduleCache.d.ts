import { ScheduleObject, Sport, Category } from '../interface';
export interface ScheduleCache<TObject extends ScheduleObject> {
    [id: string]: {
        item: TObject;
        referenceCount: number;
    };
}
export interface IScheduleCache<TObject extends ScheduleObject> {
    readonly items: {
        [id: string]: TObject;
    };
    add(item: TObject): void;
    update(item: TObject): void;
    delete(item: TObject): void;
}
export declare const _sportsCache: IScheduleCache<Sport>;
export declare const _categoriesCache: ScheduleCache<Category>;
