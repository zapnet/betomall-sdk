/**
 * @module
 * @description
 * Entry point for all public APIs of Betomall SDK
 */
export * from './src/index';
//# sourceMappingURL=index.js.map