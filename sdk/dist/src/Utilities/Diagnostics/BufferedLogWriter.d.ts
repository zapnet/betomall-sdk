import { Log, Logging } from './LoggingService';
export declare class BufferedLogWriter {
    private loggingService;
    private readonly UploadIntervalInMilliseconds;
    private readonly BufferUploadThreshold;
    private readonly BufferMaxSize;
    private buffer;
    constructor(loggingService: Logging);
    write(log: Log): void;
    private delay(ms);
    private startUploadScheduledJob();
    private isUploading;
    private isUploadFailing;
    private uploadLogs();
}
