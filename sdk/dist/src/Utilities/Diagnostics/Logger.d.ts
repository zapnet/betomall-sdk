import { BufferedLogWriter } from './BufferedLogWriter';
export declare class Logger {
    private writer;
    constructor(writer: BufferedLogWriter);
    notice(message: string): void;
    notice(error: Error): void;
    notice(message: string, error: Error): void;
    warning(message: string): void;
    warning(error: Error): void;
    warning(message: string, error: Error): void;
    error(message: string): void;
    error(error: Error): void;
    error(message: string, error: Error): void;
    private log(level, messageOrError, error?);
}
