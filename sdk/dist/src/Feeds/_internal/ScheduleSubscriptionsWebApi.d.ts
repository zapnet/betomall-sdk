import { ScheduleFilter, SportFilter, CategoryFilter } from '../interface';
export interface IScheduleSubscriptionsManager<TFilter extends ScheduleFilter> {
    modifySubscription(previous?: TFilter, current?: TFilter): Promise<SubscriptionResponse>;
}
export interface SubscriptionResponse {
    hasInitialState: boolean;
}
export declare const _sportSubscriptionsManager: IScheduleSubscriptionsManager<SportFilter>;
export declare const _categorySubscriptionsManager: IScheduleSubscriptionsManager<CategoryFilter>;
