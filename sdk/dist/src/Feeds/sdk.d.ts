import { Observable } from 'rxjs';
import { IScheduleSubscription, ScheduleState, SportFilter, Sport } from './interface';
export declare class SportSubscription implements IScheduleSubscription<SportFilter, Sport> {
    private subscription;
    readonly events: Observable<Sport>;
    readonly state: Observable<ScheduleState<Sport>>;
    private pushApiSubscription;
    constructor();
    setFilter(newFilter: SportFilter): Promise<void>;
    dispose(): void;
}
