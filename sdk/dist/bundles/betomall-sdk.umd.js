(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/http'), require('rxjs/add/operator/toPromise'), require('rxjs')) :
    typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/http', 'rxjs/add/operator/toPromise', 'rxjs'], factory) :
    (factory((global.zapnet = global.zapnet || {}, global.zapnet.betomallskd = global.zapnet.betomallskd || {}),global.ng.core,global.ng.http,global.Rx.Observable.prototype,global.rxjs));
}(this, (function (exports,_angular_core,_angular_http,rxjs_add_operator_toPromise,rxjs) { 'use strict';

//export class PushContext {
//
//    public static readonly ContextType = 'Zapnet\Utilities\API\PushContext';
//
//    private static instance?: PushContext;
//
//    public static get(): PushContext | undefined {
//        return PushContext.instance;
//    }
//
//    public static set(connectionId: string, accessToken: string) {
//        PushContext.instance = new PushContext(connectionId, accessToken);
//    }
//
//    private constructor(
//        public readonly connectionId: string,
//        public readonly accessToken: string
//    ) {}
//
//}
var ContextType = 'Zapnet\\Utilities\\API\\PushContext';
var singleton;
function get() {
    return singleton;
}
function set(connectionId, accessToken) {
    singleton = {
        connectionId: connectionId,
        accessToken: accessToken
    };
}

var WebApi = (function () {
    /**
     * If Angular Injection is used then
     * Derived classes must inject Http in their constructor
     * and pass it to WebApi base class using super(..) ==> TOO MUCH COUPLING
     *
     * Http must be encapsulated in WebApi base class.
     * - I could not find out how to instantiate it explicitly as Http constructor requires some parameters
     *      that are setup and passed by Angular during injection
     * - Another option is find a reliable Http library code and copy/paste into the project.
     *      Remove dependancy on angular/http.
     *
     * CURRENT SOLUTION: Using Angular Http + Injection.
     *  ==> All classes in the call chain must be Injectable()
     *        and exposed (even if internal) in the NgModule providers.
     */
    function WebApi(apiRelativeUrl) {
        this.apiRelativeUrl = apiRelativeUrl;
    }
    WebApi.setBaseUrl = function (webApiBaseUrl) {
        WebApi.baseUrl = webApiBaseUrl;
    };
    WebApi.prototype.call = function (methodName, args) {
        var _this = this;
        if (args === void 0) { args = {}; }
        var url = this.getUrl(methodName), request = this.createRequestEnvelope(args);
        var options = new _angular_http.RequestOptions({
            // 'Content-Type': 'text/plain' - To allow CORS without an extra OPTIONS HTTP call:
            // https://stackoverflow.com/questions/1256593/why-am-i-getting-an-options-request-instead-of-a-get-request
            headers: new _angular_http.Headers({ 'Content-Type': 'text/plain' }),
            // To pass cookies & authentication headers in CORS request.
            withCredentials: true
        });
        return _http.post(url, request, options)
            .toPromise()
            .then(function (response) {
            return _this.extractResponse(response);
        })
            .catch(function (httpError) {
            var errorMessage = _this.getHttpErrorMessage(httpError);
            throw new Error(errorMessage);
        });
    };
    WebApi.prototype.getUrl = function (methodName) {
        var url = WebApi.baseUrl + '/' + this.apiRelativeUrl + '/' + methodName;
        return url.toLowerCase();
    };
    WebApi.prototype.createRequestEnvelope = function (args) {
        var envelope = {
            headers: this.writeHeadersFromContext(),
            requestArguments: args
        };
        return envelope;
    };
    WebApi.prototype.extractResponse = function (httpResponse) {
        var envelope;
        try {
            envelope = httpResponse.json();
        }
        catch (e) {
            var error = e;
            error.message = 'Error deserializing JSON response from WebApi.\n' + error.message;
            throw error;
        }
        if (envelope.headers) {
            this.readContextFromHeaders(envelope.headers);
        }
        if (envelope.error) {
            throw new Error('WebApi returned an error:\n' + envelope.error);
        }
        return envelope.returnValue;
    };
    WebApi.prototype.getHttpErrorMessage = function (error) {
        var errMsg = 'Error during WebApi HTTP request.';
        if (error instanceof Error) {
            errMsg += '\n' + (error.message ? error.message : error.toString());
        }
        else if (error instanceof _angular_http.Response) {
            errMsg += '\n' + error.toString();
        }
        return errMsg;
    };
    WebApi.prototype.readContextFromHeaders = function (headers) {
        if (headers[ContextType]) {
            var pushContext = headers[ContextType];
            set(pushContext.connectionId, pushContext.accessToken);
        }
    };
    WebApi.prototype.writeHeadersFromContext = function () {
        var headers = {};
        var pushContext = get();
        if (pushContext) {
            headers[ContextType] = pushContext;
        }
        return headers;
    };
    WebApi.baseUrl = '';
    return WebApi;
}());
var injector = _angular_core.ReflectiveInjector.resolveAndCreate([
    _angular_http.Http,
    _angular_http.BrowserXhr,
    { provide: _angular_http.RequestOptions, useClass: _angular_http.BaseRequestOptions },
    { provide: _angular_http.ResponseOptions, useClass: _angular_http.BaseResponseOptions },
    { provide: _angular_http.ConnectionBackend, useClass: _angular_http.XHRBackend },
    { provide: _angular_http.XSRFStrategy, useFactory: function () { return new _angular_http.CookieXSRFStrategy(); } },
]);
var _http = injector.get(_angular_http.Http);

var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Logging = (function (_super) {
    __extends(Logging, _super);
    function Logging() {
        return _super.call(this, 'logging') || this;
    }
    Logging.prototype.logBatch = function (logs) {
        return this.call('logBatch', logs);
    };
    Logging.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    Logging.ctorParameters = function () { return []; };
    return Logging;
}(WebApi));

function convertToUTC(date) {
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
}

var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var BufferedLogWriter = (function () {
    function BufferedLogWriter(loggingService) {
        this.loggingService = loggingService;
        this.UploadIntervalInMilliseconds = 10000;
        this.BufferUploadThreshold = 100;
        this.BufferMaxSize = 1000;
        this.buffer = [];
        this.isUploading = false;
        this.isUploadFailing = false;
        this.startUploadScheduledJob();
    }
    BufferedLogWriter.prototype.write = function (log) {
        if (this.buffer.length >= this.BufferMaxSize) {
            console.error(BufferedLogWriter.name + ": Buffer is full.");
            return;
        }
        this.buffer.push(log);
        if (this.buffer.length >= this.BufferUploadThreshold) {
            setTimeout(this.uploadLogs, 10);
        }
    };
    BufferedLogWriter.prototype.delay = function (ms) {
        return new Promise(function (resolve) {
            setTimeout(resolve, ms);
        });
    };
    BufferedLogWriter.prototype.startUploadScheduledJob = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 2, 4]);
                        return [4 /*yield*/, this.uploadLogs()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.delay(this.UploadIntervalInMilliseconds)];
                    case 3:
                        _a.sent();
                        this.startUploadScheduledJob();
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    BufferedLogWriter.prototype.uploadLogs = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.isUploading) {
                            return [2 /*return*/];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        this.isUploading = true;
                        return [4 /*yield*/, this.loggingService.logBatch(this.buffer)];
                    case 2:
                        _a.sent();
                        this.isUploadFailing = false;
                        this.buffer = [];
                        return [3 /*break*/, 5];
                    case 3:
                        error_1 = _a.sent();
                        if (!this.isUploadFailing) {
                            this.buffer.push({
                                level: "error",
                                datetime: convertToUTC(new Date()).valueOf(),
                                text: 'Logger API - Failed to upload logs to server.',
                                exception: error_1
                            });
                        }
                        this.isUploadFailing = true;
                        return [3 /*break*/, 5];
                    case 4:
                        this.isUploading = false;
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    BufferedLogWriter.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    BufferedLogWriter.ctorParameters = function () { return [
        { type: Logging, },
    ]; };
    return BufferedLogWriter;
}());

var Logger = (function () {
    function Logger(writer) {
        this.writer = writer;
    }
    Logger.prototype.notice = function (messageOrError, error) {
        this.log("notice", messageOrError, error);
    };
    Logger.prototype.warning = function (messageOrError, error) {
        this.log("warning", messageOrError, error);
    };
    Logger.prototype.error = function (messageOrError, error) {
        this.log("error", messageOrError, error);
    };
    Logger.prototype.log = function (level, messageOrError, error) {
        var theError, theMessage;
        if (messageOrError instanceof Error) {
            theMessage = messageOrError.message;
            theError = messageOrError;
        }
        else {
            theMessage = messageOrError;
            theError = error;
        }
        var log = {
            level: level,
            datetime: convertToUTC(new Date()).valueOf(),
            text: theMessage,
            exception: theError
        };
        this.writer.write(log);
    };
    Logger.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    Logger.ctorParameters = function () { return [
        { type: BufferedLogWriter, },
    ]; };
    return Logger;
}());

var __extends$1 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter$1 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$1 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var PushApiOf = (function () {
    function PushApiOf(eventsInterfaceName) {
        this.eventsInterfaceName = eventsInterfaceName;
    }
    PushApiOf.prototype.subscribe = function (subscriber) {
        PushApi.getInstance().subscribe(this.eventsInterfaceName, subscriber);
    };
    PushApiOf.prototype.unsubscribe = function (subscriber) {
        PushApi.getInstance().unsubscribe(this.eventsInterfaceName, subscriber);
    };
    return PushApiOf;
}());
var PushApi = (function () {
    function PushApi() {
        this.subscribers = {};
    }
    PushApi.getInstance = function () {
        return PushApi.singleton;
    };
    PushApi.prototype.subscribe = function (eventsInterfaceName, subscriber) {
        var subscribers = this.subscribers[eventsInterfaceName];
        if (subscribers == null) {
            this.subscribers[eventsInterfaceName] = [subscriber];
        }
        else {
            var index = subscribers.findIndex(function (x) { return x === subscriber; });
            if (index === -1) {
                subscribers.push(subscriber);
            }
        }
    };
    PushApi.prototype.unsubscribe = function (eventsInterfaceName, subscriber) {
        var subscribers = this.subscribers[eventsInterfaceName];
        if (subscribers == null) {
            return;
        }
        var index = subscribers.findIndex(function (x) { return x === subscriber; });
        if (index !== -1) {
            subscribers.splice(index, 1);
        }
    };
    PushApi.prototype.onMessageReceived = function (message) {
        var subscribers = this.subscribers[message.interfaceName];
        if (subscribers == null) {
            return;
        }
        // TODO - shuffle subscribers to create non-deterministic publishing
        subscribers.forEach(function (subscriber) {
            if (subscriber[message.methodName]) {
                subscriber[message.methodName].apply(subscriber, message.arguments);
            }
        });
    };
    PushApi.singleton = new PushApi();
    return PushApi;
}());
var PushConnection = (function (_super) {
    __extends$1(PushConnection, _super);
    function PushConnection(http) {
        var _this = _super.call(this, 'push') || this;
        _this.pollTimer = null;
        _this.isConnected = false;
        return _this;
    }
    PushConnection.prototype.connect = function () {
        return __awaiter$1(this, void 0, void 0, function () {
            var _this = this;
            var response;
            return __generator$1(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.isConnected) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.call('connect')];
                    case 1:
                        response = _a.sent();
                        set(response.connectionId, response.accessToken);
                        if (this.pollTimer === null) {
                            this.pollTimer = setInterval(function () { return __awaiter$1(_this, void 0, void 0, function () {
                                return __generator$1(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, this.poll()];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); }, PushConnection.POLLING_INTERVAL_MS);
                        }
                        this.isConnected = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    PushConnection.prototype.disconnect = function () {
        return __awaiter$1(this, void 0, void 0, function () {
            return __generator$1(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isConnected) {
                            return [2 /*return*/];
                        }
                        if (this.pollTimer !== null) {
                            clearInterval(this.pollTimer);
                            this.pollTimer === null;
                        }
                        return [4 /*yield*/, this.call('disconnect')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PushConnection.prototype.poll = function () {
        return __awaiter$1(this, void 0, void 0, function () {
            var response, request, messageId, message;
            return __generator$1(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.lastProcessedMessageId) return [3 /*break*/, 2];
                        request = {
                            lastReceivedMessageId: this.lastProcessedMessageId,
                            lastProcessedMessageId: this.lastProcessedMessageId
                        };
                        return [4 /*yield*/, this.call('poll', request)];
                    case 1:
                        response = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.call('poll')];
                    case 3:
                        response = _a.sent();
                        _a.label = 4;
                    case 4:
                        if (!response.isQueueEmpty) {
                            this.poll();
                        }
                        for (messageId in response.events) {
                            message = response.events[messageId];
                            PushApi.getInstance().onMessageReceived(message);
                            this.lastProcessedMessageId = messageId;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    PushConnection.POLLING_INTERVAL_MS = 3000;
    PushConnection.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    PushConnection.ctorParameters = function () { return [
        { type: _angular_http.Http, },
    ]; };
    return PushConnection;
}(WebApi));

var __extends$2 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var EVENTS_INTERFACE_NAME = 'Zapnet\Managers\Betting\IBettingEvents';
var BettingEventsPushApi = (function (_super) {
    __extends$2(BettingEventsPushApi, _super);
    function BettingEventsPushApi() {
        return _super.call(this, EVENTS_INTERFACE_NAME) || this;
    }
    return BettingEventsPushApi;
}(PushApiOf));

var __extends$3 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var AccountWebApi = (function (_super) {
    __extends$3(AccountWebApi, _super);
    function AccountWebApi() {
        return _super.call(this, 'account') || this;
    }
    AccountWebApi.prototype.loadBettingHistory = function (filters, pageSize, pageNumber) {
        return this.call('loadBettingHistory', {
            filters: filters,
            pageSize: pageSize,
            pageNumber: pageNumber
        });
    };
    AccountWebApi.prototype.loadBetslipDetails = function (betslipId) {
        return this.call('loadBetslipDetails', {
            betslipId: betslipId
        });
    };
    AccountWebApi.prototype.loadAccountBalance = function () {
        return this.call('loadAccountBalance');
    };
    AccountWebApi.prototype.loadAccountHistory = function (filters, pageSize, pageNumber) {
        return this.call('loadAccountHistory', {
            filters: filters,
            pageSize: pageSize,
            pageNumber: pageNumber
        });
    };
    AccountWebApi.prototype.loadProfile = function () {
        return this.call('loadProfile');
    };
    AccountWebApi.prototype.saveProfile = function (profile) {
        return this.call('saveProfile', {
            profile: profile
        });
    };
    AccountWebApi.prototype.changePassword = function (currentPassword, newPassword, verifyNewPassword) {
        return this.call('changePassword', {
            currentPassword: currentPassword,
            newPassword: newPassword,
            verifyNewPassword: verifyNewPassword
        });
    };
    AccountWebApi.prototype.loadSecurityLogs = function (pageSize, pageNumber) {
        return this.call('loadSecurityLogs', {
            pageSize: pageSize,
            pageNumber: pageNumber
        });
    };
    AccountWebApi.prototype.loadMessageThreads = function () {
        return this.call('loadMessageThreads');
    };
    AccountWebApi.prototype.loadMessageThreadEntries = function (threadId) {
        return this.call('loadMessageThreadEntries', {
            threadId: threadId
        });
    };
    AccountWebApi.prototype.createNewMessageThread = function (subject, message) {
        return this.call('createNewMessageThread', {
            subject: subject,
            message: message
        });
    };
    AccountWebApi.prototype.postMessage = function (threadId, message) {
        return this.call('postMessage', {
            threadId: threadId,
            message: message
        });
    };
    AccountWebApi.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    AccountWebApi.ctorParameters = function () { return []; };
    return AccountWebApi;
}(WebApi));

(function (MessageThreadStatus) {
    MessageThreadStatus["OPEN"] = "open";
    MessageThreadStatus["CLOSED"] = "closed";
    MessageThreadStatus["VERIFIED"] = "verified";
})(exports.MessageThreadStatus || (exports.MessageThreadStatus = {}));

(function (MessageSender) {
    MessageSender["CUSTOMER"] = "customer";
    MessageSender["OPERATOR"] = "operator";
})(exports.MessageSender || (exports.MessageSender = {}));

(function (MessageEntryStatus) {
    MessageEntryStatus["UNREAD"] = "new";
    MessageEntryStatus["READ"] = "read";
    MessageEntryStatus["REPLIED"] = "replied";
})(exports.MessageEntryStatus || (exports.MessageEntryStatus = {}));

var BetomallSdkModule = (function () {
    function BetomallSdkModule() {
    }
    BetomallSdkModule.configure = function (config) {
        WebApi.setBaseUrl(config.serverBaseUrl);
    };
    BetomallSdkModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    imports: [_angular_http.HttpModule],
                    declarations: [],
                    exports: [],
                    providers: [
                        Logger,
                        BufferedLogWriter,
                        Logging,
                        PushConnection,
                        BettingEventsPushApi,
                        AccountWebApi
                    ]
                },] },
    ];
    /** @nocollapse */
    BetomallSdkModule.ctorParameters = function () { return []; };
    return BetomallSdkModule;
}());

var BetResult = (function () {
    function BetResult() {
    }
    BetResult.PENDING = 'pending';
    BetResult.WON = 'won';
    BetResult.LOST = 'lost';
    BetResult.CANCELLED = 'cancelled';
    return BetResult;
}());

var __extends$4 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var EVENTS_INTERFACE_NAME$1 = 'Zapnet\Managers\Feeds\IScheduleEvents';
var ScheduleEventsPushApi = (function (_super) {
    __extends$4(ScheduleEventsPushApi, _super);
    function ScheduleEventsPushApi() {
        return _super.call(this, EVENTS_INTERFACE_NAME$1) || this;
    }
    return ScheduleEventsPushApi;
}(PushApiOf));
var _schedulePushApi = new ScheduleEventsPushApi();

var __extends$5 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Filters">

(function (MatchStatusFilter) {
    MatchStatusFilter["PreGame"] = "PreGame";
    MatchStatusFilter["Live"] = "Live";
})(exports.MatchStatusFilter || (exports.MatchStatusFilter = {}));
var ScheduleFilter = (function () {
    function ScheduleFilter() {
    }
    return ScheduleFilter;
}());
var SportFilter = (function (_super) {
    __extends$5(SportFilter, _super);
    function SportFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SportFilter;
}(ScheduleFilter));
var CategoryFilter = (function (_super) {
    __extends$5(CategoryFilter, _super);
    function CategoryFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return CategoryFilter;
}(SportFilter));
var TournamentFilter = (function (_super) {
    __extends$5(TournamentFilter, _super);
    function TournamentFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return TournamentFilter;
}(CategoryFilter));
var MatchFilter = (function (_super) {
    __extends$5(MatchFilter, _super);
    function MatchFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return MatchFilter;
}(TournamentFilter));
var MarketFilter = (function (_super) {
    __extends$5(MarketFilter, _super);
    function MarketFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return MarketFilter;
}(MatchFilter));

// </editor-fold>

var ComparisonResult;
(function (ComparisonResult) {
    ComparisonResult[ComparisonResult["Incremental"] = 1] = "Incremental";
    ComparisonResult[ComparisonResult["Same"] = 0] = "Same";
    ComparisonResult[ComparisonResult["Decremental"] = -1] = "Decremental";
})(ComparisonResult || (ComparisonResult = {}));
var FilteringEngine = (function () {
    function FilteringEngine() {
    }
    FilteringEngine.doesSportPass = function (filter, sport) {
        var result = false;
        if (filter.sportIds != null && filter.sportIds.indexOf(sport.sportId) !== -1) {
            result = true;
        }
        return result;
    };
    FilteringEngine.doesCategoryPass = function (filter, category) {
        var result = FilteringEngine.doesSportPass(filter, category);
        if (filter.categoryIds != null && filter.categoryIds.indexOf(category.categoryId) !== -1) {
            result = true;
        }
        return result;
    };
    FilteringEngine.compareFilters = function (before, after) {
        var result = FilteringEngine.compareNullableObjects(before, after);
        if (result !== null) {
            return result;
        }
        //  TODO compare all levels in hierarchy
        if (before instanceof SportFilter && after instanceof SportFilter) {
        }
        throw new Error("Method not implemented.");
    };
    FilteringEngine.compareNullableObjects = function (before, after) {
        if (before == null && after == null) {
            return ComparisonResult.Same;
        }
        else if (before == null && after != null) {
            return ComparisonResult.Incremental;
        }
        else if (before == null && after == null) {
            return ComparisonResult.Decremental;
        }
        return null;
    };
    return FilteringEngine;
}());

var __extends$6 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter$3 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$3 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var ScheduleSubscriptionsManager = (function () {
    function ScheduleSubscriptionsManager() {
        this.lastFilterIndex = 0;
        this.filters = {};
        this.subscriptionVersion = 0;
    }
    ScheduleSubscriptionsManager.prototype.modifySubscription = function (previous, current) {
        return __awaiter$3(this, void 0, void 0, function () {
            var index, subscription;
            return __generator$3(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.assert(previous != null || current != null);
                        index = this.getIndexOfFilter(previous);
                        if (current != null) {
                            this.filters[index] = current;
                        }
                        else {
                            delete this.filters[index];
                        }
                        this.subscriptionVersion++;
                        subscription = {
                            filters: this.filters,
                            version: this.subscriptionVersion
                        };
                        return [4 /*yield*/, this.modifySubscriptionApiCall(subscription)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ScheduleSubscriptionsManager.prototype.getIndexOfFilter = function (filter) {
        if (filter == null) {
            this.lastFilterIndex++;
            return this.lastFilterIndex;
        }
        else {
            for (var i in this.filters) {
                if (this.filters[i] === filter) {
                    return parseInt(i);
                }
            }
        }
        throw Error("Filter index not found.");
    };
    return ScheduleSubscriptionsManager;
}());
var SportSubscriptionsManager = (function (_super) {
    __extends$6(SportSubscriptionsManager, _super);
    function SportSubscriptionsManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SportSubscriptionsManager.prototype.modifySubscriptionApiCall = function (subscription) {
        return _webApi.modifySportSubscription(subscription);
    };
    return SportSubscriptionsManager;
}(ScheduleSubscriptionsManager));
var CategorySubscriptionsManager = (function (_super) {
    __extends$6(CategorySubscriptionsManager, _super);
    function CategorySubscriptionsManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CategorySubscriptionsManager.prototype.modifySubscriptionApiCall = function (subscription) {
        return _webApi.modifyCategorySubscription(subscription);
    };
    return CategorySubscriptionsManager;
}(ScheduleSubscriptionsManager));
var ScheduleSubscriptionsWebApi = (function (_super) {
    __extends$6(ScheduleSubscriptionsWebApi, _super);
    function ScheduleSubscriptionsWebApi() {
        return _super.call(this, 'ScheduleSubscriptions') || this;
    }
    ScheduleSubscriptionsWebApi.prototype.modifySportSubscription = function (subscription) {
        return this.call('modifySportSubscription', {
            subscription: subscription
        });
    };
    ScheduleSubscriptionsWebApi.prototype.modifyCategorySubscription = function (subscription) {
        return this.call('modifyCategorySubscription', {
            subscription: subscription
        });
    };
    return ScheduleSubscriptionsWebApi;
}(WebApi));
var _webApi = new ScheduleSubscriptionsWebApi();
var _sportSubscriptionsManager = new SportSubscriptionsManager();
var _categorySubscriptionsManager = new CategorySubscriptionsManager();

var ScheduleCacheImplementation = (function () {
    function ScheduleCacheImplementation() {
        this.referenceCount = {};
    }
    ScheduleCacheImplementation.prototype.add = function (item) {
        var id = item.id;
        if (id in this.items) {
            console.assert(item === this.items[id]);
            this.referenceCount[id]++;
        }
        else {
            this.items[id] = item;
            this.referenceCount[id] = 1;
        }
    };
    ScheduleCacheImplementation.prototype.update = function (item) {
        var id = item.id;
        console.assert(id in this.items);
        if (item === this.items[id]) {
            return;
        }
        if (item.version > this.items[id].version) {
            this.items[id] = item;
        }
    };
    ScheduleCacheImplementation.prototype.delete = function (item) {
        var id = item.id;
        console.assert(id in this.items);
        this.referenceCount[id]--;
        if (this.referenceCount[id] === 0) {
            delete this.items[id];
            delete this.referenceCount[id];
        }
    };
    return ScheduleCacheImplementation;
}());
var _sportsCache = new ScheduleCacheImplementation();

var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __awaiter$2 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$2 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var STATE_DEBOUNCE_TIME_MS = 100;
var InitialScheduleState = { isInitializing: true, byId: {} };
var SportSubscription = (function () {
    function SportSubscription() {
        this.subscription = new ScheduleSubscription(_sportsCache, FilteringEngine.doesSportPass, _sportSubscriptionsManager);
        this.events = this.subscription.events;
        this.state = this.subscription.state.debounceTime(STATE_DEBOUNCE_TIME_MS);
        var subscription = this.subscription;
        this.pushApiSubscription = {
            onSportChanged: function (sport) {
                subscription.handleOnObjectChangedEvent(sport);
            }
        };
        _schedulePushApi.subscribe(this.pushApiSubscription);
    }
    SportSubscription.prototype.setFilter = function (newFilter) {
        return this.subscription.setFilter(newFilter);
    };
    SportSubscription.prototype.dispose = function () {
        _schedulePushApi.unsubscribe(this.pushApiSubscription);
        this.subscription.dispose();
    };
    return SportSubscription;
}());
var ScheduleSubscription = (function () {
    function ScheduleSubscription(cache, filteringFunction, subscriptionsManager) {
        this.cache = cache;
        this.filteringFunction = filteringFunction;
        this.subscriptionsManager = subscriptionsManager;
        this.events = new rxjs.Subject();
        this.state = new rxjs.BehaviorSubject(InitialScheduleState);
        this._state = InitialScheduleState;
        this.isDisposed = false;
    }
    ScheduleSubscription.prototype.setFilter = function (newFilter) {
        return __awaiter$2(this, void 0, void 0, function () {
            var _this = this;
            var previousFilter, filterChange, previousState, newState, id, cachedItem, previousIds, newIds, addedIds, deletedIds, subscriptionResponse;
            return __generator$2(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.throwIfDisposed();
                        previousFilter = this.filter, filterChange = FilteringEngine.compareFilters(previousFilter, newFilter);
                        if (filterChange === ComparisonResult.Same) {
                            return [2 /*return*/];
                        }
                        this.filter = newFilter;
                        previousState = this._state, newState = {
                            isInitializing: filterChange === ComparisonResult.Incremental,
                            byId: {}
                        };
                        for (id in this.cache) {
                            cachedItem = this.cache.items[id];
                            if (cachedItem.isDeleted) {
                                continue;
                            }
                            if (this.filteringFunction(newFilter, cachedItem)) {
                                newState.byId[id] = cachedItem;
                            }
                        }
                        previousIds = Object.keys(previousState.byId), newIds = Object.keys(newState.byId), addedIds = array_diff(newIds, previousIds), deletedIds = array_diff(previousIds, newIds);
                        addedIds.forEach(function (id) { return _this.cache.add(_this.cache.items[id]); });
                        deletedIds.forEach(function (id) { return _this.cache.delete(_this.cache.items[id]); });
                        if (this.events.observers.length > 0) {
                            addedIds.forEach(function (id) { return _this.publishEvent(_this.cache.items[id]); });
                            deletedIds.forEach(function (id) {
                                var deletedObject = Object.assign({}, _this.cache.items[id], {
                                    isDeleted: true,
                                    version: _this.cache.items[id].version + 1,
                                });
                                _this.publishEvent(deletedObject);
                            });
                        }
                        this.changeState(newState);
                        return [4 /*yield*/, this.subscriptionsManager.modifySubscription(previousFilter, newFilter)];
                    case 1:
                        subscriptionResponse = _a.sent();
                        if (!subscriptionResponse.hasInitialState && this._state.isInitializing) {
                            this.changeState(Object.assign({}, this._state, {
                                isInitializing: false
                            }));
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ScheduleSubscription.prototype.handleOnObjectChangedEvent = function (newObject) {
        if (this.filter == null) {
            return;
        }
        if (!this.filteringFunction(this.filter, newObject)) {
            return;
        }
        var id = newObject.id, state = this._state;
        if (id in state.byId && newObject.version <= state.byId[id].version) {
            return;
        }
        this.publishEvent(newObject);
        if (newObject.isDeleted) {
            if (!(id in state.byId)) {
                return;
            }
            var newState = Object.assign({}, state, {
                byId: Object.keys(state.byId)
                    .filter(function (key) { return key !== id; })
                    .map(function (key) {
                    return _a = {}, _a[key] = state.byId[key], _a;
                    var _a;
                })
            });
            this.changeState(newState);
            this.cache.delete(newObject);
        }
        else {
            var isAdded = !(id in state.byId), isInitializing = isAdded ? false : state.isInitializing, newState = Object.assign({}, state, /* is 'state' necessary? since the whole object is recreated. */ {
                isInitializing: isInitializing,
                byId: __assign({}, this._state.byId, (_a = {}, _a[id] = newObject, _a))
            });
            this.changeState(newState);
            if (isAdded) {
                this.cache.add(newObject);
            }
            else {
                this.cache.update(newObject);
            }
        }
        var _a;
    };
    ScheduleSubscription.prototype.publishEvent = function (newObject) {
        this.events.next(newObject);
    };
    ScheduleSubscription.prototype.changeState = function (newState) {
        this._state = newState;
        this.state.next(newState);
    };
    ScheduleSubscription.prototype.dispose = function () {
        if (this.isDisposed) {
            return;
        }
        this.subscriptionsManager.modifySubscription(this.filter, undefined);
        this.isDisposed = true;
    };
    ScheduleSubscription.prototype.throwIfDisposed = function () {
        if (this.isDisposed) {
            throw new Error('The subscription has been disposed.');
        }
    };
    return ScheduleSubscription;
}());
var array_diff = function (array1, array2) {
    return array1.filter(function (x) { return array2.indexOf(x) === -1; });
};
// Offer Standard Subscriptions?
//   NEEDS MORE ANALYSIS!!
//      move to another file
//
// export const FavouriteTournamentsSubscription = ...
// export const FavouriteCategoriesSubscription = new CategorySubscription();
// FavouriteCategoriesSubscription.setFilter(); // should be private method!!
//     filters will be returned through some Model on app initialization.
//       or maybe we don't need the filters on browser app?

var __extends$7 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var BettingWebApi = (function (_super) {
    __extends$7(BettingWebApi, _super);
    function BettingWebApi() {
        return _super.call(this, 'betting') || this;
    }
    BettingWebApi.prototype.submitBet = function (request) {
        return this.call('submitBet', {
            request: request
        });
    };
    BettingWebApi.decorators = [
        { type: _angular_core.Injectable },
    ];
    /** @nocollapse */
    BettingWebApi.ctorParameters = function () { return []; };
    return BettingWebApi;
}(WebApi));

/**
 * @module
 * @description
 * Entry point for all public APIs of Betomall SDK
 */

exports.BetomallSdkModule = BetomallSdkModule;
exports.Logger = Logger;
exports.PushConnection = PushConnection;
exports.AccountWebApi = AccountWebApi;
exports.BetResult = BetResult;
exports.SportSubscription = SportSubscription;
exports.ScheduleFilter = ScheduleFilter;
exports.SportFilter = SportFilter;
exports.CategoryFilter = CategoryFilter;
exports.TournamentFilter = TournamentFilter;
exports.MatchFilter = MatchFilter;
exports.MarketFilter = MarketFilter;
exports.BettingWebApi = BettingWebApi;
exports.BettingEventsPushApi = BettingEventsPushApi;

Object.defineProperty(exports, '__esModule', { value: true });

})));
