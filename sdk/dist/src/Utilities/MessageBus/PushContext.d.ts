export declare const ContextType = "Zapnet\\Utilities\\API\\PushContext";
export interface Type {
    connectionId: string;
    accessToken: string;
}
export declare function get(): Type | undefined;
export declare function set(connectionId: string, accessToken: string): void;
