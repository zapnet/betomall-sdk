import { Injectable } from '@angular/core';
import { BufferedLogWriter } from './BufferedLogWriter';
import { convertToUTC } from './Util';
var Logger = (function () {
    function Logger(writer) {
        this.writer = writer;
    }
    Logger.prototype.notice = function (messageOrError, error) {
        this.log("notice", messageOrError, error);
    };
    Logger.prototype.warning = function (messageOrError, error) {
        this.log("warning", messageOrError, error);
    };
    Logger.prototype.error = function (messageOrError, error) {
        this.log("error", messageOrError, error);
    };
    Logger.prototype.log = function (level, messageOrError, error) {
        var theError, theMessage;
        if (messageOrError instanceof Error) {
            theMessage = messageOrError.message;
            theError = messageOrError;
        }
        else {
            theMessage = messageOrError;
            theError = error;
        }
        var log = {
            level: level,
            datetime: convertToUTC(new Date()).valueOf(),
            text: theMessage,
            exception: theError
        };
        this.writer.write(log);
    };
    Logger.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    Logger.ctorParameters = function () { return [
        { type: BufferedLogWriter, },
    ]; };
    return Logger;
}());
export { Logger };
//# sourceMappingURL=Logger.js.map