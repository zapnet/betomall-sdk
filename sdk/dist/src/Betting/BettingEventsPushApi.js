var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { PushApiOf } from '../Utilities/MessageBus/PushApi';
var EVENTS_INTERFACE_NAME = 'Zapnet\Managers\Betting\IBettingEvents';
var BettingEventsPushApi = (function (_super) {
    __extends(BettingEventsPushApi, _super);
    function BettingEventsPushApi() {
        return _super.call(this, EVENTS_INTERFACE_NAME) || this;
    }
    return BettingEventsPushApi;
}(PushApiOf));
export { BettingEventsPushApi };
//# sourceMappingURL=BettingEventsPushApi.js.map