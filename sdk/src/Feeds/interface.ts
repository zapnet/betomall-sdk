import {Observable} from 'rxjs';

// <editor-fold defaultstate="collapsed" desc="Subscription">
export interface IScheduleSubscription<TFilter extends ScheduleFilter, TObject extends ScheduleObject> {
    readonly events: Observable<TObject>;
    readonly state: Observable<ScheduleState<TObject>>;
    setFilter(newFilter: TFilter): Promise<void>
    dispose(): void;
}

export interface ScheduleState<TObject extends ScheduleObject> {
    isInitializing: boolean,
    byId: {
        [id: string]: TObject
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Filters">
export enum MatchStatusFilter {
    PreGame = 'PreGame',
    Live = 'Live'
}

export class ScheduleFilter {
    matchStatus?: MatchStatusFilter;
}

export class SportFilter extends ScheduleFilter {
    sportIds?: string[];
}

export class CategoryFilter extends SportFilter {
    categoryIds?: string[];
}

export class TournamentFilter extends CategoryFilter {
    tournamentIds?: string[];
}

export class MatchFilter extends TournamentFilter {
    // TODO - $matchKickoffRelativePeriod
    matchIds?: string[];
}

export class MarketFilter extends MatchFilter {
    marketTypeIds?: string[];
    marketIds?: string[];
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Schedule Objects">
export interface ScheduleObject {
    id: string,
    isDeleted: boolean;
    version: number;
}

export interface Sport extends ScheduleObject {
    sportId: string;
    code: string;
    name: string;
    order: number;
    numberOfPreGameMatches: number;
    numberOfLiveMatches: number;
}

export interface Category extends ScheduleObject {
    sportId: string;
    sportName: string;
    categoryId: string;
    code: string;
    name: string;
    order: number;
    numberOfPreGameMatches: number;
    numberOfLiveMatches: number;
}



// TODO - REVISE the following:

export interface Tournament extends ScheduleObject {
    id: string;
    parentId: number;
    code: string;
    mj: number;
    name: string;
    nrm: number;
    order: number;
}

export interface Match extends ScheduleObject {
    id: string;
    code: string;
    name: string;
    tournamentId: string;
    tournamentName: string;
    categoryId: string;
    categoryName: string;
    sportId: string;
    sportName: string;

    bet: number; // ?? canBet ?
    bets: number; // ??
    competitors: {
        home: string;
        away: string;
    };
    cards: {
        home: {
            yellow: number;
            yellowred: number;
            red: number
        };
        away: {
            yellow: number;
            yellowred: number;
            red: number
        }
    };
    corners: {
        home: number;
        away: number
    };
    full: boolean;
    hidden: boolean;
    ldata: any;
    live: string;
    livebet: string;
    livecards?: any;
    livecorners?: any;
    lmtime?: string;
    ls_booked: number;
    lsrc: any;
    lstatus: any;
    mco: number;
    neutral: boolean;
    nrm: string;
    score?: string;
    setscores?: string;
    status: string;
    ts: number;
    willgolive: number;

    timestamp: number;
}


export interface Market extends ScheduleObject {
    id: string;
    matchId: string;
    name: string;
    live: boolean;
    max_payout: string;
    mincomb: string;
    order: string;
    payout: string;
    selmc: string;
    special: string;
    stake: any;
    status: string;
    type: string;
    typeid: string;

    selections: [{
        id: string;
        label: string;
        order: number;
        odds: number;
        oddsDirection: "up" | "down";
        outcome: string; // Result??
    }];
    timestamp: number;
}


export interface Outright extends ScheduleObject {
    categoryId: string;
    id: string;
    name: string;
    oddstype: string;
    action: any;
    combine: number;
    date: string;
    enabled: number;
    enddate: string;
    endtime: string;
    endts: number;
    incompatible: any;
    market: string;
    max_bet_payout?: number;
    max_bet_stake?: number;
    max_shop_payout?: number;
    max_shop_stake?: number;
    max_total_payout?: number;
    max_total_stake?: number;
    payout?: number;
    stake?: number;
    outright_info: any;
    rid: string;
    status: string;
    tid: any;
    time: string;
    tname?: string;
    ts: number;

    competitors: [{
        id: string;
        name: string;
        group: any;
        cid: string;
        outright_competitor_info: any;
        team: string;
        odds: string
    }];
    timestamp: number;
}
// </editor-fold>
