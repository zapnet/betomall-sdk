;
;
var ScheduleCacheImplementation = (function () {
    function ScheduleCacheImplementation() {
        this.referenceCount = {};
    }
    ScheduleCacheImplementation.prototype.add = function (item) {
        var id = item.id;
        if (id in this.items) {
            console.assert(item === this.items[id]);
            this.referenceCount[id]++;
        }
        else {
            this.items[id] = item;
            this.referenceCount[id] = 1;
        }
    };
    ScheduleCacheImplementation.prototype.update = function (item) {
        var id = item.id;
        console.assert(id in this.items);
        if (item === this.items[id]) {
            return;
        }
        if (item.version > this.items[id].version) {
            this.items[id] = item;
        }
    };
    ScheduleCacheImplementation.prototype.delete = function (item) {
        var id = item.id;
        console.assert(id in this.items);
        this.referenceCount[id]--;
        if (this.referenceCount[id] === 0) {
            delete this.items[id];
            delete this.referenceCount[id];
        }
    };
    return ScheduleCacheImplementation;
}());
export var _sportsCache = new ScheduleCacheImplementation();
export var _categoriesCache = {};
//# sourceMappingURL=ScheduleCache.js.map