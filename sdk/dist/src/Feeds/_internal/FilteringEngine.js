import { SportFilter } from '../interface';
export var ComparisonResult;
(function (ComparisonResult) {
    ComparisonResult[ComparisonResult["Incremental"] = 1] = "Incremental";
    ComparisonResult[ComparisonResult["Same"] = 0] = "Same";
    ComparisonResult[ComparisonResult["Decremental"] = -1] = "Decremental";
})(ComparisonResult || (ComparisonResult = {}));
var FilteringEngine = (function () {
    function FilteringEngine() {
    }
    FilteringEngine.doesSportPass = function (filter, sport) {
        var result = false;
        if (filter.sportIds != null && filter.sportIds.indexOf(sport.sportId) !== -1) {
            result = true;
        }
        return result;
    };
    FilteringEngine.doesCategoryPass = function (filter, category) {
        var result = FilteringEngine.doesSportPass(filter, category);
        if (filter.categoryIds != null && filter.categoryIds.indexOf(category.categoryId) !== -1) {
            result = true;
        }
        return result;
    };
    FilteringEngine.compareFilters = function (before, after) {
        var result = FilteringEngine.compareNullableObjects(before, after);
        if (result !== null) {
            return result;
        }
        //  TODO compare all levels in hierarchy
        if (before instanceof SportFilter && after instanceof SportFilter) {
        }
        throw new Error("Method not implemented.");
    };
    FilteringEngine.compareNullableObjects = function (before, after) {
        if (before == null && after == null) {
            return ComparisonResult.Same;
        }
        else if (before == null && after != null) {
            return ComparisonResult.Incremental;
        }
        else if (before == null && after == null) {
            return ComparisonResult.Decremental;
        }
        return null;
    };
    return FilteringEngine;
}());
export { FilteringEngine };
//# sourceMappingURL=FilteringEngine.js.map