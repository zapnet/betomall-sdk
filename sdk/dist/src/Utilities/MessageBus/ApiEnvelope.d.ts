export interface RequestEnvelope {
    headers: ContextHeaders;
    requestArguments: null | object;
}
export interface ResponseEnvelope {
    headers: ContextHeaders;
    returnValue: any;
    error: any;
}
export interface ContextHeaders {
    [contextType: string]: object;
}
