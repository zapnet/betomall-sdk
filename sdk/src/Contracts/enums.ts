
export class BetResult {
    public static readonly PENDING = 'pending';
    public static readonly WON = 'won';
    public static readonly LOST = 'lost';
    public static readonly CANCELLED = 'cancelled';
}