import {WebApi} from '../../Utilities/MessageBus/WebApi';
import {ScheduleFilter, SportFilter, CategoryFilter, TournamentFilter, MatchFilter, MarketFilter} from '../interface';

export interface IScheduleSubscriptionsManager<TFilter extends ScheduleFilter> {
    modifySubscription(previous?: TFilter, current?: TFilter): Promise<SubscriptionResponse>;
}

abstract class ScheduleSubscriptionsManager<TFilter extends ScheduleFilter>
    implements IScheduleSubscriptionsManager<TFilter> {

    private lastFilterIndex: number = 0;
    private filters: {
        [index: number]: TFilter
    } = {};
    private subscriptionVersion: number = 0;

    public async modifySubscription(previous?: TFilter, current?: TFilter): Promise<SubscriptionResponse> {
        console.assert(previous != null || current != null);
        const index = this.getIndexOfFilter(previous);
        if (current != null) {
            this.filters[index] = current;
        } else {
            delete this.filters[index];
        }
        this.subscriptionVersion++;
        const subscription: ScheduleSubscriptionApiRequest<TFilter> = {
            filters: this.filters,
            version: this.subscriptionVersion
        };
        return await this.modifySubscriptionApiCall(subscription);
    }

    protected abstract modifySubscriptionApiCall(subscription: ScheduleSubscriptionApiRequest<TFilter>): Promise<SubscriptionResponse>;

    private getIndexOfFilter(filter?: TFilter): number {
        if (filter == null) {
            this.lastFilterIndex++;
            return this.lastFilterIndex;
        } else {
            for (let i in this.filters) {
                if (this.filters[i] === filter) {
                    return parseInt(i);
                }
            }
        }
        throw Error(`Filter index not found.`);
    }
}

class SportSubscriptionsManager extends ScheduleSubscriptionsManager<SportFilter> {
    protected modifySubscriptionApiCall(subscription: ScheduleSubscriptionApiRequest<SportFilter>): Promise<SubscriptionResponse> {
        return _webApi.modifySportSubscription(subscription);
    }
}
class CategorySubscriptionsManager extends ScheduleSubscriptionsManager<CategoryFilter> {
    protected modifySubscriptionApiCall(subscription: ScheduleSubscriptionApiRequest<CategoryFilter>): Promise<SubscriptionResponse> {
        return _webApi.modifyCategorySubscription(subscription);
    }
}

class ScheduleSubscriptionsWebApi extends WebApi {

    constructor() {super('ScheduleSubscriptions');}

    public modifySportSubscription(subscription: ScheduleSubscriptionApiRequest<SportFilter>): Promise<SubscriptionResponse> {
        return this.call<SubscriptionResponse>('modifySportSubscription', {
            subscription: subscription
        });
    }

    public modifyCategorySubscription(subscription: ScheduleSubscriptionApiRequest<CategoryFilter>): Promise<SubscriptionResponse> {
        return this.call<SubscriptionResponse>('modifyCategorySubscription', {
            subscription: subscription
        });
    }
}

interface ScheduleSubscriptionApiRequest<TFilter extends ScheduleFilter> {
    filters: {
        [index: number]: TFilter
    },
    version: number
}

export interface SubscriptionResponse {
    hasInitialState: boolean;
}


const _webApi = new ScheduleSubscriptionsWebApi();

export const _sportSubscriptionsManager: IScheduleSubscriptionsManager<SportFilter> = new SportSubscriptionsManager();
export const _categorySubscriptionsManager: IScheduleSubscriptionsManager<CategoryFilter> = new CategorySubscriptionsManager();


