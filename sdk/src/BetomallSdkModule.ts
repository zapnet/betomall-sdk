import {Logger} from './Utilities/Diagnostics/Logger';
import {BufferedLogWriter} from './Utilities/Diagnostics/BufferedLogWriter';
import {Logging} from './Utilities/Diagnostics/LoggingService';
import {WebApi} from './Utilities/MessageBus/WebApi';
import {PushConnection} from './Utilities/MessageBus/PushApi'
import {BettingEventsPushApi} from './Betting/BettingEventsPushApi';
import {AccountWebApi} from './Admin/AccountWebApi';

import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';

@NgModule({
    imports: [HttpModule],
    declarations: [],
    exports: [],
    providers: [
        Logger,
        BufferedLogWriter,
        Logging,
        PushConnection,
        BettingEventsPushApi,
        AccountWebApi
    ]
})
export class BetomallSdkModule {
    public static configure(config: BetomallSdkModuleConfiguration) {
        WebApi.setBaseUrl(config.serverBaseUrl);
    }
}

export interface BetomallSdkModuleConfiguration {
    serverBaseUrl: string
}