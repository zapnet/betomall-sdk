/*
 * Each library (Angular, RxJS, ...) has its own convention for UMD modules:
 *   Angular: ng.<module name>
 *   RxJS:
 *      classes (Observable,...)         : Rx
 *      operators (map, filter,...)      : Rx.Observable.prototype
 *      Classes methods (of, fromEvent…): Rx.Observable
 *
 *
 */

export default {
    entry: 'dist/index.js',
    dest: 'dist/bundles/betomall-sdk.umd.js',
    sourceMap: false,
    format: 'umd',
    moduleName: 'zapnet.betomallskd',
    globals: {
        '@angular/core': 'ng.core',
        '@angular/http': 'ng.http',
//        'rxjs/Observable': 'Rx',
//        'rxjs/ReplaySubject': 'Rx',
        'rxjs/add/operator/toPromise': 'Rx.Observable.prototype',
//        'rxjs/add/operator/map': 'Rx.Observable.prototype',
//        'rxjs/add/operator/mergeMap': 'Rx.Observable.prototype',
//        'rxjs/add/operator/pluck': 'Rx.Observable.prototype',
//        'rxjs/add/operator/first': 'Rx.Observable.prototype',
//        'rxjs/add/observable/fromEvent': 'Rx.Observable',
//        'rxjs/add/observable/merge': 'Rx.Observable',
//        'rxjs/add/observable/throw': 'Rx.Observable',
//        'rxjs/add/observable/of': 'Rx.Observable'
    }
}

