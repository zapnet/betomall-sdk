var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable } from '@angular/core';
import { WebApi } from '../Utilities/MessageBus/WebApi';
var AccountWebApi = (function (_super) {
    __extends(AccountWebApi, _super);
    function AccountWebApi() {
        return _super.call(this, 'account') || this;
    }
    AccountWebApi.prototype.loadBettingHistory = function (filters, pageSize, pageNumber) {
        return this.call('loadBettingHistory', {
            filters: filters,
            pageSize: pageSize,
            pageNumber: pageNumber
        });
    };
    AccountWebApi.prototype.loadBetslipDetails = function (betslipId) {
        return this.call('loadBetslipDetails', {
            betslipId: betslipId
        });
    };
    AccountWebApi.prototype.loadAccountBalance = function () {
        return this.call('loadAccountBalance');
    };
    AccountWebApi.prototype.loadAccountHistory = function (filters, pageSize, pageNumber) {
        return this.call('loadAccountHistory', {
            filters: filters,
            pageSize: pageSize,
            pageNumber: pageNumber
        });
    };
    AccountWebApi.prototype.loadProfile = function () {
        return this.call('loadProfile');
    };
    AccountWebApi.prototype.saveProfile = function (profile) {
        return this.call('saveProfile', {
            profile: profile
        });
    };
    AccountWebApi.prototype.changePassword = function (currentPassword, newPassword, verifyNewPassword) {
        return this.call('changePassword', {
            currentPassword: currentPassword,
            newPassword: newPassword,
            verifyNewPassword: verifyNewPassword
        });
    };
    AccountWebApi.prototype.loadSecurityLogs = function (pageSize, pageNumber) {
        return this.call('loadSecurityLogs', {
            pageSize: pageSize,
            pageNumber: pageNumber
        });
    };
    AccountWebApi.prototype.loadMessageThreads = function () {
        return this.call('loadMessageThreads');
    };
    AccountWebApi.prototype.loadMessageThreadEntries = function (threadId) {
        return this.call('loadMessageThreadEntries', {
            threadId: threadId
        });
    };
    AccountWebApi.prototype.createNewMessageThread = function (subject, message) {
        return this.call('createNewMessageThread', {
            subject: subject,
            message: message
        });
    };
    AccountWebApi.prototype.postMessage = function (threadId, message) {
        return this.call('postMessage', {
            threadId: threadId,
            message: message
        });
    };
    AccountWebApi.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AccountWebApi.ctorParameters = function () { return []; };
    return AccountWebApi;
}(WebApi));
export { AccountWebApi };
export var MessageThreadStatus;
(function (MessageThreadStatus) {
    MessageThreadStatus["OPEN"] = "open";
    MessageThreadStatus["CLOSED"] = "closed";
    MessageThreadStatus["VERIFIED"] = "verified";
})(MessageThreadStatus || (MessageThreadStatus = {}));
export var MessageSender;
(function (MessageSender) {
    MessageSender["CUSTOMER"] = "customer";
    MessageSender["OPERATOR"] = "operator";
})(MessageSender || (MessageSender = {}));
export var MessageEntryStatus;
(function (MessageEntryStatus) {
    MessageEntryStatus["UNREAD"] = "new";
    MessageEntryStatus["READ"] = "read";
    MessageEntryStatus["REPLIED"] = "replied";
})(MessageEntryStatus || (MessageEntryStatus = {}));
//# sourceMappingURL=AccountWebApi.js.map