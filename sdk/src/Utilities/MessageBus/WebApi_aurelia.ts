import {HttpClient, HttpResponseMessage} from 'aurelia-http-client';
import {RequestEnvelope, ResponseEnvelope} from './ApiEnvelope';

import {initialize} from 'aurelia-pal-browser';
initialize();

export class WebApi {
    public static serverBaseUrl?: string = undefined;
    protected readonly apiRelativeUrl?: string = undefined;

    /**
     * If Angular Injection is used then
     * Derived classes must inject Http in their constructor
     * and pass it to WebApi base class using super(..) ==> TOO MUCH COUPLING
     *
     * Http must be encapsulated in WebApi base class.
     * - I could not find out how to instantiate it explicitly as Http constructor requires some parameters
     *      that are setup and passed by Angular during injection
     * - Another option is find a reliable Http library code and copy/paste into the project.
     *      Remove dependancy on Angular.
     *
     * CURRENT SOLUTION: Using Aurelia Http client.
     */
    private htpp: HttpClient;
    private isHttpInitialised: boolean = false;
    private getHttp() {
        if (!this.isHttpInitialised) {
            this.htpp = new HttpClient();
            this.isHttpInitialised = true;
        }
        return this.htpp;
    }
    public constructor() {
        this.apiRelativeUrl = this.apiRelativeUrl || this.getDefaultBaseUrl();
    }

    private getDefaultBaseUrl(): string {
        const className: string = this.constructor.name;
        return className;
    }

    protected call<TResponse>(methodName: string, ...args: any[]): Promise<TResponse> {
        const url = this.getUrl(methodName),
            request = this.createRequestEnvelope(args);
        return this.getHttp().post(url, request)
            .then((response: HttpResponseMessage) => {
                return this.extractResponse<TResponse>(response);
            })
            .catch((error: any) => {
                const exception: Error = this.convertHttpErrorToException(error);
                throw exception;
            });
    }

    private getUrl(methodName: string) {
        return (WebApi.serverBaseUrl || '') + '/' + this.apiRelativeUrl + '/' + methodName;
    }

    private createRequestEnvelope(args: any[]): RequestEnvelope {
        let envelope: RequestEnvelope = {
            headers: [],
            requestArguments: args
        };
        return envelope;
    }
    private extractResponse<TResponse>(httpResponse: HttpResponseMessage): TResponse {
        let envelope: ResponseEnvelope = JSON.parse(httpResponse.response);
        if (envelope.error) {
            throw new Error(envelope.error);
        }
        return (envelope.returnValue || null) as TResponse;
    }

    private convertHttpErrorToException(error: HttpResponseMessage | any): Error {
        let errMsg: string = 'Unknown error during Http request.';
        if (error instanceof HttpResponseMessage) {
            errMsg = `${error.statusCode} - ${error.statusText || ''}`;
        } else if (error instanceof Error) {
            errMsg = error.message ? error.message : error.toString();
        }
        return new Error(errMsg);
    }
}

