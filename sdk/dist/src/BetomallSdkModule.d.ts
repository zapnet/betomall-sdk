export declare class BetomallSdkModule {
    static configure(config: BetomallSdkModuleConfiguration): void;
}
export interface BetomallSdkModuleConfiguration {
    serverBaseUrl: string;
}
