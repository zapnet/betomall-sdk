import {Injectable} from '@angular/core';

import {WebApi} from '../Utilities/MessageBus/WebApi';
import {BetResult} from '../Contracts/enums';
import {PagedResultset} from '../Contracts/Types';

@Injectable()
export class AccountWebApi extends WebApi {
    constructor() {
        super('account');
    }
    loadBettingHistory(filters?: BettingHistoryFilters, pageSize?: number, pageNumber?: number): Promise<PagedResultset<BetslipSummary>> {
        return this.call<PagedResultset<BetslipSummary>>('loadBettingHistory', {
            filters: filters,
            pageSize: pageSize,
            pageNumber: pageNumber
        });
    }
    loadBetslipDetails(betslipId: string): Promise<string> {
        return this.call<string>('loadBetslipDetails', {
            betslipId: betslipId
        });
    }
    loadAccountBalance(): Promise<AccountBalance> {
        return this.call<AccountBalance>('loadAccountBalance');
    }
    loadAccountHistory(filters?: AccountHistoryFilters, pageSize?: number, pageNumber?: number): Promise<PagedResultset<TransactionSummary>> {
        return this.call<PagedResultset<TransactionSummary>>('loadAccountHistory', {
            filters: filters,
            pageSize: pageSize,
            pageNumber: pageNumber
        })
    }
    loadProfile(): Promise<Profile> {
        return this.call<Profile>('loadProfile');
    }
    saveProfile(profile: Profile): Promise<RequestValidationResult> {
        return this.call<RequestValidationResult>('saveProfile', {
            profile: profile
        });
    }
    changePassword(currentPassword: string, newPassword: string, verifyNewPassword: string): Promise<RequestValidationResult> {
        return this.call<RequestValidationResult>('changePassword', {
            currentPassword: currentPassword,
            newPassword: newPassword,
            verifyNewPassword: verifyNewPassword
        });
    }
    loadSecurityLogs(pageSize?: number, pageNumber?: number): Promise<PagedResultset<SecurityLog>> {
        return this.call<PagedResultset<SecurityLog>>('loadSecurityLogs', {
            pageSize: pageSize,
            pageNumber: pageNumber
        })
    }
    loadMessageThreads(): Promise<MessageThread> {
        return this.call<MessageThread>('loadMessageThreads');
    }
    loadMessageThreadEntries(threadId: number): Promise<MessageThreadEntry> {
        return this.call<MessageThreadEntry>('loadMessageThreadEntries', {
            threadId: threadId
        });
    }
    createNewMessageThread(subject: string, message: string): Promise<RequestValidationResult> {
        return this.call<RequestValidationResult>('createNewMessageThread', {
            subject: subject,
            message: message
        });
    }
    postMessage(threadId: number, message: string): Promise<RequestValidationResult> {
        return this.call<RequestValidationResult>('postMessage', {
            threadId: threadId,
            message: message
        });
    }
}

export interface BettingHistoryFilters {
    betResult?: BetResult;
    product?: string;
    fromDateTime?: string;
    toDateTime?: string;
}

export interface BetslipSummary {
    betslipId: string;
    betResult: BetResult;
    totalLines: number;
    datePlaced: string;
    description: string;
    stake: number;
    winningsPaid: number;
    possibleBonus: number;
    possibleWinnings: number;
}

export interface AccountBalance {
    balance: number;
    availableBalanceToBet?: number;
    availableBalanceToWithdraw?: number;
    reservedToWithdraw?: number;
    bonus?: number;
}

export interface AccountHistoryFilters {
    fromDateTime?: string;
    toDateTime?: string;
}

export interface TransactionSummary {
    transactionId: string;
    createdDateTime: string;
    betslipId: string;
    amount: number;
    balanceAfterTransaction: number;
    comments: string;
}

export interface Profile {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    dateOfBirth: string;
    telephone: number;
    addressLine1: string;
    addressLine2: string;
    city: string;
    stateOrProvince: string;
    postCode: string;
    country: string;
}

export interface RequestValidationResult {
    isRequestValid: boolean;
    validationErrorMessages: string[];
}

export interface SecurityLog {
    loginDateTime: string;
    loginIP: string;
    logoutDateTime: string;
    failedLoginReason: string;
}

export interface MessageThread {
    threadId: string;
    subject: string;
    status: MessageThreadStatus;
    lastUpdatedDate: string;
    numberOfMessages: number;
    numberOfUnreadMessages: number;
}

export enum MessageThreadStatus {
    OPEN = 'open',
    CLOSED = 'closed',
    VERIFIED = 'verified'
}

export interface MessageThreadEntry {
    entryNumber: number;
    sender: MessageSender;
    status: MessageEntryStatus;
    sentDate: string;
    message: string;
}

export enum MessageSender {
    CUSTOMER = 'customer',
    OPERATOR = 'operator'
}

export enum MessageEntryStatus {
    UNREAD = 'new',
    READ = 'read',
    REPLIED = 'replied'
}