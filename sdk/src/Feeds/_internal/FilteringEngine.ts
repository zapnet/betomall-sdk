import {ScheduleFilter, SportFilter, CategoryFilter, TournamentFilter, MatchFilter, MarketFilter} from '../interface';

export enum ComparisonResult {
    Incremental = 1,
    Same = 0,
    Decremental = -1
}

export class FilteringEngine {

    public static doesSportPass(filter: SportFilter, sport: {sportId: string}): boolean {
        let result = false;
        if (filter.sportIds != null && filter.sportIds.indexOf(sport.sportId) !== -1) {
            result = true;
        }
        return result;
    }

    public static doesCategoryPass(filter: CategoryFilter, category: {sportId: string, categoryId: string}): boolean {
        let result = FilteringEngine.doesSportPass(filter, category);
        if (filter.categoryIds != null && filter.categoryIds.indexOf(category.categoryId) !== -1) {
            result = true;
        }
        return result;
    }

    public static compareFilters(before?: ScheduleFilter, after?: ScheduleFilter): ComparisonResult {
        let result = FilteringEngine.compareNullableObjects(before, after);
        if (result !== null) {
            return result;
        }
        //  TODO compare all levels in hierarchy
        if (before instanceof SportFilter && after instanceof SportFilter) {


        }

        throw new Error("Method not implemented.");
    }

    private static compareNullableObjects(before?: any, after?: any): ComparisonResult | null {
        if (before == null && after == null) {
            return ComparisonResult.Same;
        } else if (before == null && after != null) {
            return ComparisonResult.Incremental;
        } else if (before == null && after == null) {
            return ComparisonResult.Decremental;
        }
        return null;
    }

}

