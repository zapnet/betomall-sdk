

//export class PushContext {
//
//    public static readonly ContextType = 'Zapnet\Utilities\API\PushContext';
//
//    private static instance?: PushContext;
//
//    public static get(): PushContext | undefined {
//        return PushContext.instance;
//    }
//
//    public static set(connectionId: string, accessToken: string) {
//        PushContext.instance = new PushContext(connectionId, accessToken);
//    }
//
//    private constructor(
//        public readonly connectionId: string,
//        public readonly accessToken: string
//    ) {}
//
//}



export const ContextType = 'Zapnet\\Utilities\\API\\PushContext';

export interface Type {
    connectionId: string;
    accessToken: string;
}
let singleton: Type;

export function get(): Type | undefined {
    return singleton;
}

export function set(connectionId: string, accessToken: string) {
    singleton = {
        connectionId: connectionId,
        accessToken: accessToken
    }
}