import 'rxjs/add/operator/toPromise';
export declare class WebApi {
    private readonly apiRelativeUrl;
    private static baseUrl;
    static setBaseUrl(webApiBaseUrl: string): void;
    /**
     * If Angular Injection is used then
     * Derived classes must inject Http in their constructor
     * and pass it to WebApi base class using super(..) ==> TOO MUCH COUPLING
     *
     * Http must be encapsulated in WebApi base class.
     * - I could not find out how to instantiate it explicitly as Http constructor requires some parameters
     *      that are setup and passed by Angular during injection
     * - Another option is find a reliable Http library code and copy/paste into the project.
     *      Remove dependancy on angular/http.
     *
     * CURRENT SOLUTION: Using Angular Http + Injection.
     *  ==> All classes in the call chain must be Injectable()
     *        and exposed (even if internal) in the NgModule providers.
     */
    constructor(apiRelativeUrl: string);
    protected call<TResponse>(methodName: string, args?: object): Promise<TResponse>;
    private getUrl(methodName);
    private createRequestEnvelope(args);
    private extractResponse<TResponse>(httpResponse);
    private getHttpErrorMessage(error);
    private readContextFromHeaders(headers);
    private writeHeadersFromContext();
}
