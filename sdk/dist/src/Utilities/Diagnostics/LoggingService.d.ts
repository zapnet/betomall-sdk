import { WebApi } from '../MessageBus/WebApi';
export declare type LogLevel = "notice" | "warning" | "error";
export interface Log {
    level: LogLevel;
    datetime: number;
    text: string;
    exception?: Error;
}
export interface ILogging {
    logBatch(logs: Log[]): Promise<void>;
}
export declare class Logging extends WebApi implements ILogging {
    constructor();
    logBatch(logs: Log[]): Promise<void>;
}
