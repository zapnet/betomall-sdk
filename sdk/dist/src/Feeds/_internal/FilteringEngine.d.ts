import { ScheduleFilter, SportFilter, CategoryFilter } from '../interface';
export declare enum ComparisonResult {
    Incremental = 1,
    Same = 0,
    Decremental = -1,
}
export declare class FilteringEngine {
    static doesSportPass(filter: SportFilter, sport: {
        sportId: string;
    }): boolean;
    static doesCategoryPass(filter: CategoryFilter, category: {
        sportId: string;
        categoryId: string;
    }): boolean;
    static compareFilters(before?: ScheduleFilter, after?: ScheduleFilter): ComparisonResult;
    private static compareNullableObjects(before?, after?);
}
