import {Observable, Subject, BehaviorSubject} from 'rxjs';
import {
    IScheduleSubscription, ScheduleState,
    MatchStatusFilter, ScheduleFilter, SportFilter, CategoryFilter, TournamentFilter, MatchFilter, MarketFilter,
    ScheduleObject, Sport, Category, Tournament, Match, Market, Outright
} from './interface';
import {_schedulePushApi, ISportChangedEvent, ICategoryChangedEvent} from './_internal/ScheduleEventsPushApi';
import {FilteringEngine, ComparisonResult} from './_internal/FilteringEngine';
import {IScheduleSubscriptionsManager, _sportSubscriptionsManager} from './_internal/ScheduleSubscriptionsWebApi';
import {IScheduleCache, _sportsCache, _categoriesCache} from './_internal/ScheduleCache';

const STATE_DEBOUNCE_TIME_MS = 100;
const InitialScheduleState: ScheduleState<any> = {isInitializing: true, byId: {}};

export class SportSubscription implements IScheduleSubscription<SportFilter, Sport> {
    private subscription = new ScheduleSubscription<SportFilter, Sport>(
        _sportsCache,
        FilteringEngine.doesSportPass,
        _sportSubscriptionsManager);

    readonly events: Observable<Sport> = this.subscription.events;
    readonly state: Observable<ScheduleState<Sport>> = this.subscription.state.debounceTime(STATE_DEBOUNCE_TIME_MS);

    private pushApiSubscription: ISportChangedEvent;

    constructor() {
        const subscription = this.subscription;
        this.pushApiSubscription = {
            onSportChanged(sport: Sport): void {
                subscription.handleOnObjectChangedEvent(sport);
            }
        };
        _schedulePushApi.subscribe(this.pushApiSubscription);
    }

    setFilter(newFilter: SportFilter): Promise<void> {
        return this.subscription.setFilter(newFilter);
    }

    dispose(): void {
        _schedulePushApi.unsubscribe(this.pushApiSubscription);
        this.subscription.dispose();
    }
}

class ScheduleSubscription<TFilter extends ScheduleFilter, TObject extends ScheduleObject> {

    public readonly events = new Subject<TObject>();
    public readonly state = new BehaviorSubject<ScheduleState<TObject>>(InitialScheduleState);

    private _state: ScheduleState<TObject> = InitialScheduleState;
    private filter?: TFilter;
    private isDisposed = false;

    public constructor(
        private cache: IScheduleCache<TObject>,
        private filteringFunction: (filter: TFilter, event: TObject) => boolean,
        private subscriptionsManager: IScheduleSubscriptionsManager<TFilter>
    ) {}

    public async setFilter(newFilter: TFilter): Promise<void> {
        this.throwIfDisposed();
        const
            previousFilter = this.filter,
            filterChange = FilteringEngine.compareFilters(previousFilter, newFilter);
        if (filterChange === ComparisonResult.Same) {
            return;
        }
        this.filter = newFilter;

        const
            previousState = this._state,
            newState: ScheduleState<TObject> = {
                isInitializing: filterChange === ComparisonResult.Incremental,
                byId: {}
            };

        for (let id in this.cache) {
            const cachedItem = this.cache.items[id];
            if (cachedItem.isDeleted) {
                continue;
            }
            if (this.filteringFunction(newFilter, cachedItem)) {
                newState.byId[id] = cachedItem;
            }
        }

        const
            previousIds = Object.keys(previousState.byId),
            newIds = Object.keys(newState.byId),
            addedIds = array_diff(newIds, previousIds),
            deletedIds = array_diff(previousIds, newIds);
        addedIds.forEach(id => this.cache.add(this.cache.items[id]));
        deletedIds.forEach(id => this.cache.delete(this.cache.items[id]));

        if (this.events.observers.length > 0) {
            addedIds.forEach(id => this.publishEvent(this.cache.items[id]));
            deletedIds.forEach(id => {
                const deletedObject = Object.assign({}, this.cache.items[id], {
                    isDeleted: true,
                    version: this.cache.items[id].version + 1,
                });
                this.publishEvent(deletedObject);
            });
        }
        this.changeState(newState);

        const subscriptionResponse = await this.subscriptionsManager.modifySubscription(previousFilter, newFilter);
        if (!subscriptionResponse.hasInitialState && this._state.isInitializing) {
            this.changeState(Object.assign({}, this._state, {
                isInitializing: false
            }));
        }
    }

    public handleOnObjectChangedEvent(newObject: TObject): void {
        if (this.filter == null) {
            return;
        }
        if (!this.filteringFunction(this.filter, newObject)) {
            return;
        }
        const
            id = newObject.id,
            state = this._state;

        if (id in state.byId && newObject.version <= state.byId[id].version) {
            return;
        }

        this.publishEvent(newObject);

        if (newObject.isDeleted) {
            if (!(id in state.byId)) {
                return;
            }
            const newState: ScheduleState<TObject> = Object.assign({}, state, {
                byId: Object.keys(state.byId)
                    .filter(key => key !== id)
                    .map(key => {
                        return {[key]: state.byId[key]}
                    })
            });
            this.changeState(newState);
            this.cache.delete(newObject);
        } else {
            const isAdded = !(id in state.byId),
                isInitializing = isAdded ? false : state.isInitializing,
                newState: ScheduleState<TObject> = Object.assign({}, state, /* is 'state' necessary? since the whole object is recreated. */ {
                    isInitializing: isInitializing,
                    byId: {
                        ...this._state.byId,
                        [id]: newObject
                    }
                });
            this.changeState(newState);
            if (isAdded) {
                this.cache.add(newObject);
            } else {
                this.cache.update(newObject);
            }
        }
    }

    private publishEvent(newObject: TObject) {
        this.events.next(newObject);
    }

    private changeState(newState: ScheduleState<TObject>) {
        this._state = newState;
        this.state.next(newState);
    }

    public dispose(): void {
        if (this.isDisposed) {
            return;
        }
        this.subscriptionsManager.modifySubscription(this.filter, undefined);
        this.isDisposed = true;
    }

    private throwIfDisposed() {
        if (this.isDisposed) {
            throw new Error('The subscription has been disposed.');
        }
    }
}

const array_diff = function <T>(array1: Array<T>, array2: Array<T>) {
    return array1.filter(x => array2.indexOf(x) === -1);
};



// Offer Standard Subscriptions?
//   NEEDS MORE ANALYSIS!!
//      move to another file
//
// export const FavouriteTournamentsSubscription = ...
// export const FavouriteCategoriesSubscription = new CategorySubscription();
// FavouriteCategoriesSubscription.setFilter(); // should be private method!!
//     filters will be returned through some Model on app initialization.
//       or maybe we don't need the filters on browser app?

