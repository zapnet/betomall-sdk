import {WebApi} from './WebApi';
import {Http} from '@angular/http';
import {Injectable} from '@angular/core';
import * as PushContext from './PushContext';

export class PushApiOf<TEvents> {
    protected constructor(private eventsInterfaceName: string) {}

    public subscribe(subscriber: TEvents) {
        PushApi.getInstance().subscribe(this.eventsInterfaceName, subscriber);
    }

    public unsubscribe(subscriber: TEvents) {
        PushApi.getInstance().unsubscribe(this.eventsInterfaceName, subscriber);
    }
}

class PushApi {

    private static singleton: PushApi = new PushApi();
    public static getInstance(): PushApi {
        return PushApi.singleton;
    }

    private readonly subscribers: {
        [eventsInterfaceName: string]: Array<{
            [methodName: string]: (...args: any[]) => void;
        }>;
    } = {};

    private constructor() {}

    public subscribe(eventsInterfaceName: string, subscriber: any): void {
        let subscribers = this.subscribers[eventsInterfaceName];
        if (subscribers == null) {
            this.subscribers[eventsInterfaceName] = [subscriber];
        } else {
            const index = subscribers.findIndex(x => x === subscriber);
            if (index === -1) {
                subscribers.push(subscriber);
            }
        }
    }

    public unsubscribe(eventsInterfaceName: string, subscriber: any): void {
        let subscribers = this.subscribers[eventsInterfaceName];
        if (subscribers == null) {
            return;
        }
        const index = subscribers.findIndex(x => x === subscriber);
        if (index !== -1) {
            subscribers.splice(index, 1);
        }
    }

    public onMessageReceived(message: PushMessage) {
        const subscribers = this.subscribers[message.interfaceName];
        if (subscribers == null) {
            return;
        }
        // TODO - shuffle subscribers to create non-deterministic publishing
        subscribers.forEach(subscriber => {
            if (subscriber[message.methodName]) {
                subscriber[message.methodName](...message.arguments);
            }
        });
    }
}

@Injectable()
export class PushConnection extends WebApi {

    private static readonly POLLING_INTERVAL_MS = 3000;
    private pollTimer: any = null;
    private isConnected: boolean = false;
    private lastProcessedMessageId?: string;

    constructor(http: Http) {
        super('push');
    }

    public async connect(): Promise<void> {
        if (this.isConnected) {
            return;
        }
        const response = await this.call<ConnectResponse>('connect');
        PushContext.set(response.connectionId, response.accessToken);
        if (this.pollTimer === null) {
            this.pollTimer = setInterval(async () => {
                await this.poll();
            }, PushConnection.POLLING_INTERVAL_MS);
        }
        this.isConnected = true;
    }

    public async disconnect(): Promise<void> {
        if (!this.isConnected) {
            return;
        }
        if (this.pollTimer !== null) {
            clearInterval(this.pollTimer);
            this.pollTimer === null;
        }
        await this.call<void>('disconnect');
    }

    private async poll(): Promise<void> {
        // TODO - Retry failed http request?
        let response: PollResponse;
        if (this.lastProcessedMessageId) {
            const request: PollRequest = {
                lastReceivedMessageId: this.lastProcessedMessageId,
                lastProcessedMessageId: this.lastProcessedMessageId
            };
            response = await this.call<PollResponse>('poll', request);
        } else {
            response = await this.call<PollResponse>('poll');
        }
        if (!response.isQueueEmpty) {
            this.poll();
        }
        for (let messageId in response.events) {
            const message = response.events[messageId];
            PushApi.getInstance().onMessageReceived(message);
            this.lastProcessedMessageId = messageId;
        }
    }
}

interface ConnectResponse {
    connectionId: string,
    accessToken: string
}

interface PollRequest {
    lastReceivedMessageId: string;
    lastProcessedMessageId: string;
}

interface PollResponse {
    events: PushMessages;
    isQueueEmpty: boolean;
}

interface PushMessages {
    [messageId: string]: PushMessage;
}

interface PushMessage {
    interfaceName: string;
    methodName: string;
    arguments: any[];
}

/*

const betslip: Betslip = {
    betslipId: '10',
    stake: 100,
    selections: [],
    odds: 2.5,
    blahblah: ''
};
const mockPollResponse: PollResponse = {
    events: {
        1: {
            interfaceName: 'Zapnet\Managers\Betting\IBettingEvents',
            methodName: 'onBetAccepted',
            arguments: [betslip]
        },
        2: {
            interfaceName: 'Zapnet\Managers\Betting\IBettingEvents',
            methodName: 'onBetWaitingAuthorisation',
            arguments: [betslip.betslipId, 1000]
        },

    },
    isQueueEmpty: true
}

*/