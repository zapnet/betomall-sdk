var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Filters">
export var MatchStatusFilter;
(function (MatchStatusFilter) {
    MatchStatusFilter["PreGame"] = "PreGame";
    MatchStatusFilter["Live"] = "Live";
})(MatchStatusFilter || (MatchStatusFilter = {}));
var ScheduleFilter = (function () {
    function ScheduleFilter() {
    }
    return ScheduleFilter;
}());
export { ScheduleFilter };
var SportFilter = (function (_super) {
    __extends(SportFilter, _super);
    function SportFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SportFilter;
}(ScheduleFilter));
export { SportFilter };
var CategoryFilter = (function (_super) {
    __extends(CategoryFilter, _super);
    function CategoryFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return CategoryFilter;
}(SportFilter));
export { CategoryFilter };
var TournamentFilter = (function (_super) {
    __extends(TournamentFilter, _super);
    function TournamentFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return TournamentFilter;
}(CategoryFilter));
export { TournamentFilter };
var MatchFilter = (function (_super) {
    __extends(MatchFilter, _super);
    function MatchFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return MatchFilter;
}(TournamentFilter));
export { MatchFilter };
var MarketFilter = (function (_super) {
    __extends(MarketFilter, _super);
    function MarketFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return MarketFilter;
}(MatchFilter));
export { MarketFilter };
// </editor-fold>
//# sourceMappingURL=interface.js.map