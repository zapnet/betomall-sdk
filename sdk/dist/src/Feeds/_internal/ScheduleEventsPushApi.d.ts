import { PushApiOf } from '../../Utilities/MessageBus/PushApi';
import { Sport, Category } from '../interface';
export declare class ScheduleEventsPushApi extends PushApiOf<IScheduleEvents> {
    constructor();
}
export interface IScheduleEvents extends ISportChangedEvent, ICategoryChangedEvent {
}
export interface ISportChangedEvent {
    onSportChanged?: (sport: Sport) => void;
}
export interface ICategoryChangedEvent {
    onCategoryChanged?: (category: Category) => void;
}
export declare const _schedulePushApi: ScheduleEventsPushApi;
