import {PushApiOf} from '../../Utilities/MessageBus/PushApi';
import {Sport, Category, Tournament, Match, Market, Outright} from '../interface';

const EVENTS_INTERFACE_NAME = 'Zapnet\Managers\Feeds\IScheduleEvents';

export class ScheduleEventsPushApi extends PushApiOf<IScheduleEvents> {
    public constructor() {
        super(EVENTS_INTERFACE_NAME);
    }
}

export interface IScheduleEvents extends
    ISportChangedEvent,
    ICategoryChangedEvent
{}

export interface ISportChangedEvent {
    onSportChanged?: (sport: Sport) => void;
}

export interface ICategoryChangedEvent {
    onCategoryChanged?: (category: Category) => void;
}

export const _schedulePushApi = new ScheduleEventsPushApi();