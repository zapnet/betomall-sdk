import { Logger } from './Utilities/Diagnostics/Logger';
import { BufferedLogWriter } from './Utilities/Diagnostics/BufferedLogWriter';
import { Logging } from './Utilities/Diagnostics/LoggingService';
import { WebApi } from './Utilities/MessageBus/WebApi';
import { PushConnection } from './Utilities/MessageBus/PushApi';
import { BettingEventsPushApi } from './Betting/BettingEventsPushApi';
import { AccountWebApi } from './Admin/AccountWebApi';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
var BetomallSdkModule = (function () {
    function BetomallSdkModule() {
    }
    BetomallSdkModule.configure = function (config) {
        WebApi.setBaseUrl(config.serverBaseUrl);
    };
    BetomallSdkModule.decorators = [
        { type: NgModule, args: [{
                    imports: [HttpModule],
                    declarations: [],
                    exports: [],
                    providers: [
                        Logger,
                        BufferedLogWriter,
                        Logging,
                        PushConnection,
                        BettingEventsPushApi,
                        AccountWebApi
                    ]
                },] },
    ];
    /** @nocollapse */
    BetomallSdkModule.ctorParameters = function () { return []; };
    return BetomallSdkModule;
}());
export { BetomallSdkModule };
//# sourceMappingURL=BetomallSdkModule.js.map