import { WebApi } from '../Utilities/MessageBus/WebApi';
import { BetResult } from '../Contracts/enums';
import { PagedResultset } from '../Contracts/Types';
export declare class AccountWebApi extends WebApi {
    constructor();
    loadBettingHistory(filters?: BettingHistoryFilters, pageSize?: number, pageNumber?: number): Promise<PagedResultset<BetslipSummary>>;
    loadBetslipDetails(betslipId: string): Promise<string>;
    loadAccountBalance(): Promise<AccountBalance>;
    loadAccountHistory(filters?: AccountHistoryFilters, pageSize?: number, pageNumber?: number): Promise<PagedResultset<TransactionSummary>>;
    loadProfile(): Promise<Profile>;
    saveProfile(profile: Profile): Promise<RequestValidationResult>;
    changePassword(currentPassword: string, newPassword: string, verifyNewPassword: string): Promise<RequestValidationResult>;
    loadSecurityLogs(pageSize?: number, pageNumber?: number): Promise<PagedResultset<SecurityLog>>;
    loadMessageThreads(): Promise<MessageThread>;
    loadMessageThreadEntries(threadId: number): Promise<MessageThreadEntry>;
    createNewMessageThread(subject: string, message: string): Promise<RequestValidationResult>;
    postMessage(threadId: number, message: string): Promise<RequestValidationResult>;
}
export interface BettingHistoryFilters {
    betResult?: BetResult;
    product?: string;
    fromDateTime?: string;
    toDateTime?: string;
}
export interface BetslipSummary {
    betslipId: string;
    betResult: BetResult;
    totalLines: number;
    datePlaced: string;
    description: string;
    stake: number;
    winningsPaid: number;
    possibleBonus: number;
    possibleWinnings: number;
}
export interface AccountBalance {
    balance: number;
    availableBalanceToBet?: number;
    availableBalanceToWithdraw?: number;
    reservedToWithdraw?: number;
    bonus?: number;
}
export interface AccountHistoryFilters {
    fromDateTime?: string;
    toDateTime?: string;
}
export interface TransactionSummary {
    transactionId: string;
    createdDateTime: string;
    betslipId: string;
    amount: number;
    balanceAfterTransaction: number;
    comments: string;
}
export interface Profile {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    dateOfBirth: string;
    telephone: number;
    addressLine1: string;
    addressLine2: string;
    city: string;
    stateOrProvince: string;
    postCode: string;
    country: string;
}
export interface RequestValidationResult {
    isRequestValid: boolean;
    validationErrorMessages: string[];
}
export interface SecurityLog {
    loginDateTime: string;
    loginIP: string;
    logoutDateTime: string;
    failedLoginReason: string;
}
export interface MessageThread {
    threadId: string;
    subject: string;
    status: MessageThreadStatus;
    lastUpdatedDate: string;
    numberOfMessages: number;
    numberOfUnreadMessages: number;
}
export declare enum MessageThreadStatus {
    OPEN = "open",
    CLOSED = "closed",
    VERIFIED = "verified",
}
export interface MessageThreadEntry {
    entryNumber: number;
    sender: MessageSender;
    status: MessageEntryStatus;
    sentDate: string;
    message: string;
}
export declare enum MessageSender {
    CUSTOMER = "customer",
    OPERATOR = "operator",
}
export declare enum MessageEntryStatus {
    UNREAD = "new",
    READ = "read",
    REPLIED = "replied",
}
