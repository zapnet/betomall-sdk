//export class PushContext {
//
//    public static readonly ContextType = 'Zapnet\Utilities\API\PushContext';
//
//    private static instance?: PushContext;
//
//    public static get(): PushContext | undefined {
//        return PushContext.instance;
//    }
//
//    public static set(connectionId: string, accessToken: string) {
//        PushContext.instance = new PushContext(connectionId, accessToken);
//    }
//
//    private constructor(
//        public readonly connectionId: string,
//        public readonly accessToken: string
//    ) {}
//
//}
export var ContextType = 'Zapnet\\Utilities\\API\\PushContext';
var singleton;
export function get() {
    return singleton;
}
export function set(connectionId, accessToken) {
    singleton = {
        connectionId: connectionId,
        accessToken: accessToken
    };
}
//# sourceMappingURL=PushContext.js.map