import {ScheduleObject, Sport, Category, Tournament, Match, Market, Outright} from '../interface';

export interface ScheduleCache<TObject extends ScheduleObject> {
    [id: string]: {
        item: TObject,
        referenceCount: number
    };
};

export interface IScheduleCache<TObject extends ScheduleObject> {
    readonly items: {[id: string]: TObject};
    add(item: TObject): void;
    update(item: TObject): void;
    delete(item: TObject): void;
};

class ScheduleCacheImplementation<TObject extends ScheduleObject> implements IScheduleCache<TObject> {

    public readonly items: {[id: string]: TObject};

    private referenceCount: {[id: string]: number} = {};

    add(item: TObject): void {
        const id = item.id;
        if (id in this.items) {
            console.assert(item === this.items[id]);
            this.referenceCount[id]++;
        } else {
            this.items[id] = item;
            this.referenceCount[id] = 1;
        }
    }
    update(item: TObject): void {
        const id = item.id;
        console.assert(id in this.items);
        if (item === this.items[id]) {
            return;
        }
        if (item.version > this.items[id].version) {
            this.items[id] = item;
        }
    }
    delete(item: TObject): void {
        const id = item.id;
        console.assert(id in this.items);
        this.referenceCount[id]--;
        if (this.referenceCount[id] === 0) {
            delete this.items[id];
            delete this.referenceCount[id];
        }
    }
}

export const _sportsCache: IScheduleCache<Sport> = new ScheduleCacheImplementation<Sport>();
export const _categoriesCache: ScheduleCache<Category> = {};
