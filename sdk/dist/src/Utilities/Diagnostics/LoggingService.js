var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable } from '@angular/core';
import { WebApi } from '../MessageBus/WebApi';
var Logging = (function (_super) {
    __extends(Logging, _super);
    function Logging() {
        return _super.call(this, 'logging') || this;
    }
    Logging.prototype.logBatch = function (logs) {
        return this.call('logBatch', logs);
    };
    Logging.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    Logging.ctorParameters = function () { return []; };
    return Logging;
}(WebApi));
export { Logging };
//# sourceMappingURL=LoggingService.js.map