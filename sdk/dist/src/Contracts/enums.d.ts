export declare class BetResult {
    static readonly PENDING: string;
    static readonly WON: string;
    static readonly LOST: string;
    static readonly CANCELLED: string;
}
