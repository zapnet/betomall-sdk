import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {NgModule} from '@angular/core';
import {BetomallSdkModule, PushConnection, AccountWebApi} from 'betomall-sdk';
//import {BetomallSdkModule} from '../../../sdk/src/BetomallSdkModule'; does NOT work !!

//import {HttpClient, HttpClientXsrfModule} from '@angular/common/http';


import {AppComponent} from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        BetomallSdkModule,

        //        HttpClientXsrfModule.withOptions({
        //            cookieName: 'test'
        //        })
    ],
    providers: [PushConnection, AccountWebApi],
    bootstrap: [AppComponent]
})
export class AppModule {
    public constructor(push: PushConnection) {
        BetomallSdkModule.configure({
            serverBaseUrl: 'http://www.bet.com/api'
        });
        // push.connect();
    }
}
