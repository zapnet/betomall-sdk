
export interface PagedResultset<TRow> {
    totalRows: number;
    pageNumber: number;
    pageSize: number;
    pageRows: TRow[];
}