import {Injectable} from '@angular/core';
import {BufferedLogWriter} from './BufferedLogWriter';
import {LogLevel, Log} from './LoggingService';
import {convertToUTC} from './Util';

@Injectable()
export class Logger {
    constructor(private writer: BufferedLogWriter) {}

    public notice(message: string): void
    public notice(error: Error): void
    public notice(message: string, error: Error): void
    public notice(messageOrError: string | Error, error?: Error): void {
        this.log("notice", messageOrError, error);
    }

    public warning(message: string): void
    public warning(error: Error): void
    public warning(message: string, error: Error): void
    public warning(messageOrError: string | Error, error?: Error): void {
        this.log("warning", messageOrError, error);
    }

    public error(message: string): void
    public error(error: Error): void
    public error(message: string, error: Error): void
    public error(messageOrError: string | Error, error?: Error): void {
        this.log("error", messageOrError, error);
    }

    private log(level: LogLevel, messageOrError: string | Error, error?: Error) {
        let theError: Error | undefined,
            theMessage: string;
        if (messageOrError instanceof Error) {
            theMessage = messageOrError.message;
            theError = messageOrError;
        } else {
            theMessage = messageOrError;
            theError = error;
        }
        let log: Log = {
            level: level,
            datetime: convertToUTC(new Date()).valueOf(),
            text: theMessage,
            exception: theError
        };
        this.writer.write(log);
    }
}

