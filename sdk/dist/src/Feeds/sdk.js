var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Subject, BehaviorSubject } from 'rxjs';
import { _schedulePushApi } from './_internal/ScheduleEventsPushApi';
import { FilteringEngine, ComparisonResult } from './_internal/FilteringEngine';
import { _sportSubscriptionsManager } from './_internal/ScheduleSubscriptionsWebApi';
import { _sportsCache } from './_internal/ScheduleCache';
var STATE_DEBOUNCE_TIME_MS = 100;
var InitialScheduleState = { isInitializing: true, byId: {} };
var SportSubscription = (function () {
    function SportSubscription() {
        this.subscription = new ScheduleSubscription(_sportsCache, FilteringEngine.doesSportPass, _sportSubscriptionsManager);
        this.events = this.subscription.events;
        this.state = this.subscription.state.debounceTime(STATE_DEBOUNCE_TIME_MS);
        var subscription = this.subscription;
        this.pushApiSubscription = {
            onSportChanged: function (sport) {
                subscription.handleOnObjectChangedEvent(sport);
            }
        };
        _schedulePushApi.subscribe(this.pushApiSubscription);
    }
    SportSubscription.prototype.setFilter = function (newFilter) {
        return this.subscription.setFilter(newFilter);
    };
    SportSubscription.prototype.dispose = function () {
        _schedulePushApi.unsubscribe(this.pushApiSubscription);
        this.subscription.dispose();
    };
    return SportSubscription;
}());
export { SportSubscription };
var ScheduleSubscription = (function () {
    function ScheduleSubscription(cache, filteringFunction, subscriptionsManager) {
        this.cache = cache;
        this.filteringFunction = filteringFunction;
        this.subscriptionsManager = subscriptionsManager;
        this.events = new Subject();
        this.state = new BehaviorSubject(InitialScheduleState);
        this._state = InitialScheduleState;
        this.isDisposed = false;
    }
    ScheduleSubscription.prototype.setFilter = function (newFilter) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var previousFilter, filterChange, previousState, newState, id, cachedItem, previousIds, newIds, addedIds, deletedIds, subscriptionResponse;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.throwIfDisposed();
                        previousFilter = this.filter, filterChange = FilteringEngine.compareFilters(previousFilter, newFilter);
                        if (filterChange === ComparisonResult.Same) {
                            return [2 /*return*/];
                        }
                        this.filter = newFilter;
                        previousState = this._state, newState = {
                            isInitializing: filterChange === ComparisonResult.Incremental,
                            byId: {}
                        };
                        for (id in this.cache) {
                            cachedItem = this.cache.items[id];
                            if (cachedItem.isDeleted) {
                                continue;
                            }
                            if (this.filteringFunction(newFilter, cachedItem)) {
                                newState.byId[id] = cachedItem;
                            }
                        }
                        previousIds = Object.keys(previousState.byId), newIds = Object.keys(newState.byId), addedIds = array_diff(newIds, previousIds), deletedIds = array_diff(previousIds, newIds);
                        addedIds.forEach(function (id) { return _this.cache.add(_this.cache.items[id]); });
                        deletedIds.forEach(function (id) { return _this.cache.delete(_this.cache.items[id]); });
                        if (this.events.observers.length > 0) {
                            addedIds.forEach(function (id) { return _this.publishEvent(_this.cache.items[id]); });
                            deletedIds.forEach(function (id) {
                                var deletedObject = Object.assign({}, _this.cache.items[id], {
                                    isDeleted: true,
                                    version: _this.cache.items[id].version + 1,
                                });
                                _this.publishEvent(deletedObject);
                            });
                        }
                        this.changeState(newState);
                        return [4 /*yield*/, this.subscriptionsManager.modifySubscription(previousFilter, newFilter)];
                    case 1:
                        subscriptionResponse = _a.sent();
                        if (!subscriptionResponse.hasInitialState && this._state.isInitializing) {
                            this.changeState(Object.assign({}, this._state, {
                                isInitializing: false
                            }));
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ScheduleSubscription.prototype.handleOnObjectChangedEvent = function (newObject) {
        if (this.filter == null) {
            return;
        }
        if (!this.filteringFunction(this.filter, newObject)) {
            return;
        }
        var id = newObject.id, state = this._state;
        if (id in state.byId && newObject.version <= state.byId[id].version) {
            return;
        }
        this.publishEvent(newObject);
        if (newObject.isDeleted) {
            if (!(id in state.byId)) {
                return;
            }
            var newState = Object.assign({}, state, {
                byId: Object.keys(state.byId)
                    .filter(function (key) { return key !== id; })
                    .map(function (key) {
                    return _a = {}, _a[key] = state.byId[key], _a;
                    var _a;
                })
            });
            this.changeState(newState);
            this.cache.delete(newObject);
        }
        else {
            var isAdded = !(id in state.byId), isInitializing = isAdded ? false : state.isInitializing, newState = Object.assign({}, state, /* is 'state' necessary? since the whole object is recreated. */ {
                isInitializing: isInitializing,
                byId: __assign({}, this._state.byId, (_a = {}, _a[id] = newObject, _a))
            });
            this.changeState(newState);
            if (isAdded) {
                this.cache.add(newObject);
            }
            else {
                this.cache.update(newObject);
            }
        }
        var _a;
    };
    ScheduleSubscription.prototype.publishEvent = function (newObject) {
        this.events.next(newObject);
    };
    ScheduleSubscription.prototype.changeState = function (newState) {
        this._state = newState;
        this.state.next(newState);
    };
    ScheduleSubscription.prototype.dispose = function () {
        if (this.isDisposed) {
            return;
        }
        this.subscriptionsManager.modifySubscription(this.filter, undefined);
        this.isDisposed = true;
    };
    ScheduleSubscription.prototype.throwIfDisposed = function () {
        if (this.isDisposed) {
            throw new Error('The subscription has been disposed.');
        }
    };
    return ScheduleSubscription;
}());
var array_diff = function (array1, array2) {
    return array1.filter(function (x) { return array2.indexOf(x) === -1; });
};
// Offer Standard Subscriptions?
//   NEEDS MORE ANALYSIS!!
//      move to another file
//
// export const FavouriteTournamentsSubscription = ...
// export const FavouriteCategoriesSubscription = new CategorySubscription();
// FavouriteCategoriesSubscription.setFilter(); // should be private method!!
//     filters will be returned through some Model on app initialization.
//       or maybe we don't need the filters on browser app?
//# sourceMappingURL=sdk.js.map