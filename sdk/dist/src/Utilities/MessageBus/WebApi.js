import { Http, ConnectionBackend, XHRBackend, RequestOptions, BaseRequestOptions, Headers, Response, BrowserXhr, ResponseOptions, BaseResponseOptions, XSRFStrategy, CookieXSRFStrategy } from '@angular/http';
import { ReflectiveInjector } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import * as PushContext from './PushContext';
var WebApi = (function () {
    /**
     * If Angular Injection is used then
     * Derived classes must inject Http in their constructor
     * and pass it to WebApi base class using super(..) ==> TOO MUCH COUPLING
     *
     * Http must be encapsulated in WebApi base class.
     * - I could not find out how to instantiate it explicitly as Http constructor requires some parameters
     *      that are setup and passed by Angular during injection
     * - Another option is find a reliable Http library code and copy/paste into the project.
     *      Remove dependancy on angular/http.
     *
     * CURRENT SOLUTION: Using Angular Http + Injection.
     *  ==> All classes in the call chain must be Injectable()
     *        and exposed (even if internal) in the NgModule providers.
     */
    function WebApi(apiRelativeUrl) {
        this.apiRelativeUrl = apiRelativeUrl;
    }
    WebApi.setBaseUrl = function (webApiBaseUrl) {
        WebApi.baseUrl = webApiBaseUrl;
    };
    WebApi.prototype.call = function (methodName, args) {
        var _this = this;
        if (args === void 0) { args = {}; }
        var url = this.getUrl(methodName), request = this.createRequestEnvelope(args);
        var options = new RequestOptions({
            // 'Content-Type': 'text/plain' - To allow CORS without an extra OPTIONS HTTP call:
            // https://stackoverflow.com/questions/1256593/why-am-i-getting-an-options-request-instead-of-a-get-request
            headers: new Headers({ 'Content-Type': 'text/plain' }),
            // To pass cookies & authentication headers in CORS request.
            withCredentials: true
        });
        return _http.post(url, request, options)
            .toPromise()
            .then(function (response) {
            return _this.extractResponse(response);
        })
            .catch(function (httpError) {
            var errorMessage = _this.getHttpErrorMessage(httpError);
            throw new Error(errorMessage);
        });
    };
    WebApi.prototype.getUrl = function (methodName) {
        var url = WebApi.baseUrl + '/' + this.apiRelativeUrl + '/' + methodName;
        return url.toLowerCase();
    };
    WebApi.prototype.createRequestEnvelope = function (args) {
        var envelope = {
            headers: this.writeHeadersFromContext(),
            requestArguments: args
        };
        return envelope;
    };
    WebApi.prototype.extractResponse = function (httpResponse) {
        var envelope;
        try {
            envelope = httpResponse.json();
        }
        catch (e) {
            var error = e;
            error.message = 'Error deserializing JSON response from WebApi.\n' + error.message;
            throw error;
        }
        if (envelope.headers) {
            this.readContextFromHeaders(envelope.headers);
        }
        if (envelope.error) {
            throw new Error('WebApi returned an error:\n' + envelope.error);
        }
        return envelope.returnValue;
    };
    WebApi.prototype.getHttpErrorMessage = function (error) {
        var errMsg = 'Error during WebApi HTTP request.';
        if (error instanceof Error) {
            errMsg += '\n' + (error.message ? error.message : error.toString());
        }
        else if (error instanceof Response) {
            errMsg += '\n' + error.toString();
        }
        return errMsg;
    };
    WebApi.prototype.readContextFromHeaders = function (headers) {
        if (headers[PushContext.ContextType]) {
            var pushContext = headers[PushContext.ContextType];
            PushContext.set(pushContext.connectionId, pushContext.accessToken);
        }
    };
    WebApi.prototype.writeHeadersFromContext = function () {
        var headers = {};
        var pushContext = PushContext.get();
        if (pushContext) {
            headers[PushContext.ContextType] = pushContext;
        }
        return headers;
    };
    WebApi.baseUrl = '';
    return WebApi;
}());
export { WebApi };
var injector = ReflectiveInjector.resolveAndCreate([
    Http,
    BrowserXhr,
    { provide: RequestOptions, useClass: BaseRequestOptions },
    { provide: ResponseOptions, useClass: BaseResponseOptions },
    { provide: ConnectionBackend, useClass: XHRBackend },
    { provide: XSRFStrategy, useFactory: function () { return new CookieXSRFStrategy(); } },
]);
var _http = injector.get(Http);
//# sourceMappingURL=WebApi.js.map