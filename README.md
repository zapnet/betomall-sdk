# Betomall SDK for Angular (User Guide)

## Prerequisites

Git must be installed on your development machine, as npm cannot use SourceTree.

https://git-scm.com/




## Getting started

Add betomall-sdk as an npm dependency in the package.json of your app. 
Add the semantic version range or number at the end. 
[See npm docs](https://docs.npmjs.com/files/package.json#urls-as-dependencies) for details on specifying the Git URLs.

```json
  "dependencies": {
    ...
    "betomall-sdk": "git+https://bitbucket.org/zapnet/betomall-sdk.git#0.2.0"
  }
```

Import the betomall-sdk ngModule in your app's **root** module. 
The SDK provides only angular global services, so do NOT re-import it in your own sub modules.
If you import it more than once you will have multiple instances of the SDK services.

Import also the Angular HttpModule as betomall-sdk has a peer dependency on it.

```
...
import {HttpModule} from '@angular/http';
import {BetomallSdkModule} from 'betomall-sdk';

@NgModule({
  imports: [
    HttpModule,
    BetomallSdkModule
    ...
  ]
  ...
})
export class AppModule {}
```

Configure BetomallSdkModule in the main.ts file before bootstrapping Angular, 
to specify the base URL of the remote betomall server.
```typescript
...
import {BetomallSdkModule} from 'betomall-sdk';

...

BetomallSdkModule.configure({
    serverBaseUrl: 'http://www.bet.com/api'
});

platformBrowserDynamic().bootstrapModule(AppModule);
```


Inject the required SDK service in your component or service:

```typescript
import {AccountWebApi, BetResult} from 'betomall-sdk';

@Injectable()
export class MyServiceOrComponent {

  public constructor(private accountsApi: AccountWebApi) {}

  public async callApi() {
    // await the result
    const betsStatement = await this.accountsApi.loadBettingHistory({
      betResult: BetResult.PENDING
    });
    
    // Use betsStatement result
    ...
  }

}
```


## API

### Web API

All Web Api service methods return a generic `Promise<TResult>` where `TResult` is the type of the return value.
This allows leveraging Typescript's `async/await` when calling the Web API as shown in the previous example.

##### AccountWebApi
TODO

### Push API

TODO



## Browser support


## AoT and Universal support

This module supports [AoT pre-compiling](https://angular.io/docs/ts/latest/cookbook/aot-compiler.html)
and [Universal server-side rendering](https://github.com/angular/universal).

## Changelog

[Changelog available here](./sdk/CHANGELOG.md).

## Roadmap

TODO