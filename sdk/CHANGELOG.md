# Changelog

## 0.2.0 

- Added support for passing cookies in CORS requests
- `eventArguments` in envelope message posted to server has been converted from array to object, where property name is argument name. Imrproved debugging experience



## 0.1.0 

Added the following APIs:
- AccountWebApi
    - loadBettingHistory
    - loadBetslipDetails

