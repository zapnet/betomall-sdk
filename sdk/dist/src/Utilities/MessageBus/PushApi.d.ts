import { WebApi } from './WebApi';
import { Http } from '@angular/http';
export declare class PushApiOf<TEvents> {
    private eventsInterfaceName;
    protected constructor(eventsInterfaceName: string);
    subscribe(subscriber: TEvents): void;
    unsubscribe(subscriber: TEvents): void;
}
export declare class PushConnection extends WebApi {
    private static readonly POLLING_INTERVAL_MS;
    private pollTimer;
    private isConnected;
    private lastProcessedMessageId?;
    constructor(http: Http);
    connect(): Promise<void>;
    disconnect(): Promise<void>;
    private poll();
}
