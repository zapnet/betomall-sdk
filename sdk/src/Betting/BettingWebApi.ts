import {Injectable} from '@angular/core';

import {WebApi} from '../Utilities/MessageBus/WebApi';

@Injectable()
export class BettingWebApi extends WebApi {

    constructor() {super('betting');}

    public submitBet(request: SubmitBetRequest): Promise<void> {
        return this.call<void>('submitBet', {
            request: request
        });
    }

}

export interface SubmitBetRequest {
    selections: any[],
    stake: number,
    blah: string,
    blahblah: string
}