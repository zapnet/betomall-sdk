import {Injectable} from '@angular/core';
import {Log, Logging} from './LoggingService';
import {convertToUTC} from './Util';

@Injectable()
export class BufferedLogWriter {
    private readonly UploadIntervalInMilliseconds = 10000;
    private readonly BufferUploadThreshold = 100;
    private readonly BufferMaxSize = 1000;

    private buffer: Log[] = [];

    public constructor(private loggingService: Logging) {
        this.startUploadScheduledJob();
    }

    public write(log: Log) {
        if (this.buffer.length >= this.BufferMaxSize) {
            console.error(BufferedLogWriter.name + ": Buffer is full.");
            return;
        }
        this.buffer.push(log);
        if (this.buffer.length >= this.BufferUploadThreshold) {
            setTimeout(this.uploadLogs, 10);
        }
    }

    private delay(ms: number): Promise<void> {
        return new Promise<void>(function (resolve) {
            setTimeout(resolve, ms);
        });
    }

    private async startUploadScheduledJob() {
        try {
            await this.uploadLogs();
        } finally {
            await this.delay(this.UploadIntervalInMilliseconds);
            this.startUploadScheduledJob();
        }
    }

    private isUploading: boolean = false;
    private isUploadFailing: boolean = false;
    private async uploadLogs() {
        if (this.isUploading) {
            return;
        }
        try {
            this.isUploading = true;
            await this.loggingService.logBatch(this.buffer);
            this.isUploadFailing = false;
            this.buffer = [];
        } catch (error) {
            if (!this.isUploadFailing) { // Log only once, if failing repeatedly.
                this.buffer.push({
                    level: "error",
                    datetime: convertToUTC(new Date()).valueOf(),
                    text: 'Logger API - Failed to upload logs to server.',
                    exception: error
                });
            }
            this.isUploadFailing = true;
        } finally {
            this.isUploading = false;
        }
    }
}


