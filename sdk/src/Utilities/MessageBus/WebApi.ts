import {
    Http, ConnectionBackend, XHRBackend, RequestOptions, BaseRequestOptions, Headers, Response,
    BrowserXhr, ResponseOptions, BaseResponseOptions, XSRFStrategy, CookieXSRFStrategy
} from '@angular/http';
import {ReflectiveInjector} from '@angular/core';
import 'rxjs/add/operator/toPromise';

import {RequestEnvelope, ResponseEnvelope, ContextHeaders} from './ApiEnvelope';
import * as PushContext from './PushContext';


export class WebApi {

    private static baseUrl: string = '';
    public static setBaseUrl(webApiBaseUrl: string) {
        WebApi.baseUrl = webApiBaseUrl;
    }

    /**
     * If Angular Injection is used then
     * Derived classes must inject Http in their constructor
     * and pass it to WebApi base class using super(..) ==> TOO MUCH COUPLING
     *
     * Http must be encapsulated in WebApi base class.
     * - I could not find out how to instantiate it explicitly as Http constructor requires some parameters
     *      that are setup and passed by Angular during injection
     * - Another option is find a reliable Http library code and copy/paste into the project.
     *      Remove dependancy on angular/http.
     *
     * CURRENT SOLUTION: Using Angular Http + Injection.
     *  ==> All classes in the call chain must be Injectable()
     *        and exposed (even if internal) in the NgModule providers.
     */
    public constructor(private readonly apiRelativeUrl: string) {}

    protected call<TResponse>(methodName: string, args: object = {}): Promise<TResponse> {
        const url = this.getUrl(methodName),
            request = this.createRequestEnvelope(args);
        const options = new RequestOptions({
            // 'Content-Type': 'text/plain' - To allow CORS without an extra OPTIONS HTTP call:
            // https://stackoverflow.com/questions/1256593/why-am-i-getting-an-options-request-instead-of-a-get-request
            headers: new Headers({'Content-Type': 'text/plain'}),
            // To pass cookies & authentication headers in CORS request.
            withCredentials: true
        });

        return _http.post(url, request, options)
            .toPromise()
            .then((response: Response) => {
                return this.extractResponse<TResponse>(response);
            })
            .catch((httpError: any) => {
                const errorMessage = this.getHttpErrorMessage(httpError);
                throw new Error(errorMessage);
            });
    }

    private getUrl(methodName: string) {
        let url = WebApi.baseUrl + '/' + this.apiRelativeUrl + '/' + methodName;
        return url.toLowerCase();
    }

    private createRequestEnvelope(args: object): RequestEnvelope {
        let envelope: RequestEnvelope = {
            headers: this.writeHeadersFromContext(),
            requestArguments: args
        };
        return envelope;
    }

    private extractResponse<TResponse>(httpResponse: Response): TResponse {
        let envelope: ResponseEnvelope;
        try {
            envelope = httpResponse.json();
        } catch (e) {
            const error: Error = e;
            error.message = 'Error deserializing JSON response from WebApi.\n' + error.message;
            throw error;
        }
        if (envelope.headers) {
            this.readContextFromHeaders(envelope.headers);
        }
        if (envelope.error) {
            throw new Error('WebApi returned an error:\n' + envelope.error);
        }
        return envelope.returnValue as TResponse;
    }

    private getHttpErrorMessage(error: Response | any): string {
        let errMsg: string = 'Error during WebApi HTTP request.';
        if (error instanceof Error) {
            errMsg += '\n' + (error.message ? error.message : error.toString());
        } else if (error instanceof Response) {
            errMsg += '\n' + error.toString();
        }
        return errMsg;
    }

    private readContextFromHeaders(headers: ContextHeaders): void {
        if (headers[PushContext.ContextType]) {
            const pushContext: PushContext.Type = headers[PushContext.ContextType] as PushContext.Type;
            PushContext.set(pushContext.connectionId, pushContext.accessToken);
        }
    }

    private writeHeadersFromContext(): ContextHeaders {
        let headers: ContextHeaders = {};
        const pushContext = PushContext.get();
        if (pushContext) {
            headers[PushContext.ContextType] = pushContext;
        }
        return headers;
    }
}

const injector = ReflectiveInjector.resolveAndCreate([
    Http,
    BrowserXhr,
    {provide: RequestOptions, useClass: BaseRequestOptions},
    {provide: ResponseOptions, useClass: BaseResponseOptions},
    {provide: ConnectionBackend, useClass: XHRBackend},
    {provide: XSRFStrategy, useFactory: () => new CookieXSRFStrategy()},
]);
const _http = injector.get(Http) as Http;