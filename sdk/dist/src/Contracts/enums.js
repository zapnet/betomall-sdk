var BetResult = (function () {
    function BetResult() {
    }
    BetResult.PENDING = 'pending';
    BetResult.WON = 'won';
    BetResult.LOST = 'lost';
    BetResult.CANCELLED = 'cancelled';
    return BetResult;
}());
export { BetResult };
//# sourceMappingURL=enums.js.map