var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable } from '@angular/core';
import { WebApi } from '../Utilities/MessageBus/WebApi';
var BettingWebApi = (function (_super) {
    __extends(BettingWebApi, _super);
    function BettingWebApi() {
        return _super.call(this, 'betting') || this;
    }
    BettingWebApi.prototype.submitBet = function (request) {
        return this.call('submitBet', {
            request: request
        });
    };
    BettingWebApi.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    BettingWebApi.ctorParameters = function () { return []; };
    return BettingWebApi;
}(WebApi));
export { BettingWebApi };
//# sourceMappingURL=BettingWebApi.js.map