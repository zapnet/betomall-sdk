import { PushApiOf } from '../Utilities/MessageBus/PushApi';
export declare class BettingEventsPushApi extends PushApiOf<IBettingEvents> {
    constructor();
}
export interface IBettingEvents {
    onBetAccepted?(acceptedBetslip: Betslip): void;
    onBetRejected?(reason: RejectionReason): void;
    onBetWaitingAuthorisation?(betslipId: string, amountNeedingAuthorization: number): void;
}
export interface Betslip {
    betslipId: string;
    selections: any[];
    stake: number;
    odds: number;
    blahblah: string;
}
export interface RejectionReason {
    message: string;
}
