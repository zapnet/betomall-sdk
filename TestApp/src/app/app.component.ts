import {Component} from '@angular/core';
import {AccountWebApi, BetResult, MatchStatusFilter, SportSubscription, Sport} from 'betomall-sdk';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    public constructor(private accountsApi: AccountWebApi) {}

    public async callApiTest() {
        const history = await this.accountsApi.loadAccountHistory();
        console.log(history);
    }

    private async testScheduleSubscription() {
        // Setup subscription handler - 3 Options
        const subscription1 = new SportSubscription();

        subscription1.events.subscribe({
            next: (sport: Sport) => {

            }
        });
        subscription1.state.subscribe({
            next: (state) => {
                const isInitializing: boolean = state.isInitializing;
                const sports: {[id: string]: Sport} = state.byId;
                //
            }
        });

        // Setup filters
        subscription1.setFilter({
            sportIds: ["1", "2"],
            matchStatus: MatchStatusFilter.PreGame
        });

        // possible stream of sub.state observable:
        //     {isInitializing: true, schedule: [] }
        //
        //     {isInitializing: false, schedule: ... }


        // change filters at a later time
        setTimeout(() => {
            subscription1.setFilter({
                sportIds: ["1", "2", "3"]
            });
            // possible stream of sub.state observable:
            //     {isInitializing: true, schedule: [previous state] }
            //
            //     {isInitializing: false, schedule: ... }
        });

        // change filters at a later time
        setTimeout(() => {
            subscription1.setFilter({
                sportIds: ["1", "3"]
            });
            // stream of sub.state observable:
            //     {isInitializing: true, schedule: [previous state] }
            //     {isInitializing: false, schedule: [filtered, previous state] }
        });
    }
}
