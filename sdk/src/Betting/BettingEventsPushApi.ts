import {PushApiOf} from '../Utilities/MessageBus/PushApi';

const EVENTS_INTERFACE_NAME = 'Zapnet\Managers\Betting\IBettingEvents';

export class BettingEventsPushApi extends PushApiOf<IBettingEvents> {
    public constructor() {
        super(EVENTS_INTERFACE_NAME);
    }
}

export interface IBettingEvents {
    onBetAccepted?(acceptedBetslip: Betslip): void;
    onBetRejected?(reason: RejectionReason): void;
    onBetWaitingAuthorisation?(betslipId: string, amountNeedingAuthorization: number): void;
}


export interface Betslip {
    betslipId: string,
    selections: any[],
    stake: number,
    odds: number,
    blahblah: string
}

export interface RejectionReason {
    message: string
}