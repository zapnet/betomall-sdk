import { WebApi } from '../Utilities/MessageBus/WebApi';
export declare class BettingWebApi extends WebApi {
    constructor();
    submitBet(request: SubmitBetRequest): Promise<void>;
}
export interface SubmitBetRequest {
    selections: any[];
    stake: number;
    blah: string;
    blahblah: string;
}
